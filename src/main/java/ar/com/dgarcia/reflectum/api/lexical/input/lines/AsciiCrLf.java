package ar.com.dgarcia.reflectum.api.lexical.input.lines;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiCr;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiLf;

/**
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiCrLf extends LineTerminator {

    /**
     * @return The first character in the sequence
     */
    AsciiCr firstCharacter();

    /**
     * @return The second character of the sequence
     */
    AsciiLf secondCharacter();
}
