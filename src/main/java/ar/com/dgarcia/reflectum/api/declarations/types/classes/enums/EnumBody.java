package ar.com.dgarcia.reflectum.api.declarations.types.classes.enums;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftCurly;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightCurly;

import java.util.Optional;

/**
 * This type represents the body of an enum type
 * Created by kfgodel on 13/08/14.
 */
public interface EnumBody {

    /**
     * @return The opening body symbol
     */
    AsciiLeftCurly opening();

    /**
     * @return The list of constants
     */
    Optional<EnumConstantList> constants();

    /**
     * @return Separator symbol
     */
    Optional<AsciiComma> separator();

    /**
     * @return Declaration of bodies
     */
    Optional<EnumBodyDeclarations> bodyDeclaration();

    /**
     * @return The closing symbol
     */
    AsciiRightCurly closing();
}
