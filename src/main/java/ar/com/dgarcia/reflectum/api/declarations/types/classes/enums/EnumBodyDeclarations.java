package ar.com.dgarcia.reflectum.api.declarations.types.classes.enums;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.ClassBodyDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiSemicolon;

import java.util.stream.Stream;

/**
 * This type represents the body of a enum type declaration
 * Created by kfgodel on 14/08/14.
 */
public interface EnumBodyDeclarations {
    /**
     * @return Separator symbol from the constants section
     */
    AsciiSemicolon constantSeparator();

    /**
     * @return Declarations in the enum body
     */
    Stream<ClassBodyDeclaration> bodyDeclarations();
}
