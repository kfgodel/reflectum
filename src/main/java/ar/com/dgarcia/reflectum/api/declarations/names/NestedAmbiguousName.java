package ar.com.dgarcia.reflectum.api.declarations.names;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents a nested name for an ambiguous expression
 * Created by kfgodel on 09/08/14.
 */
public interface NestedAmbiguousName extends AmbiguousName, NestedName {

    AmbiguousName parentName();
}
