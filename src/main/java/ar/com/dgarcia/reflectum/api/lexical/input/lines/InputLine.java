package ar.com.dgarcia.reflectum.api.lexical.input.lines;

import java.util.stream.Stream;

/**
 * This type is an artificial type created to represent the concept of line that is not formally defined in the spec http://docs.oracle.com/javase/specs/jls/se8/html/jls-3.html
 * Created by kfgodel on 03/08/14.
 */
public interface InputLine {

    /**
     * @return The characters that form the line content
     */
    Stream<InputCharacter> contents();

    /**
     * @return The symbol that acts as line terminator
     */
    LineTerminator terminator();
}
