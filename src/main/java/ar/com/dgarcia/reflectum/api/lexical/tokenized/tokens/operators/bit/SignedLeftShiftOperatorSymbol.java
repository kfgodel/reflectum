package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiLt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the signed left shift operator symbol "<<"
 * Created by kfgodel on 05/08/14.
 */
public interface SignedLeftShiftOperatorSymbol extends OperatorSymbol {

    AsciiLt firstCharacter();
    AsciiLt secondCharacter();
}

