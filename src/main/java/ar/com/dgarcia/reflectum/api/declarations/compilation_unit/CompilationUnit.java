package ar.com.dgarcia.reflectum.api.declarations.compilation_unit;

import ar.com.dgarcia.reflectum.api.declarations.packages.PackageDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.imports.ImportDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.types.TypeDeclaration;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents a Java compilation unit. A file that can be compiled with javac
 * Created by kfgodel on 03/08/14.
 */
public interface CompilationUnit {

    /**
     * @return The package name declaration for this unit
     */
    Optional<PackageDeclaration> packageDeclaration();

    /**
     * @return The set of import declarations
     */
    Stream<ImportDeclaration> importDeclarations();

    /**
     * @return The set of type declarations in this compilation unit
     */
    Stream<TypeDeclaration> typeDeclarations();
}
