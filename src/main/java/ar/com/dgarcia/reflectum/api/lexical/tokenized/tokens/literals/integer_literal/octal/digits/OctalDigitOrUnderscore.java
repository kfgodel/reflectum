package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits;

/**
 * This type represents an octal digit or underscore
 * Created by kfgodel on 03/08/14.
 */
public interface OctalDigitOrUnderscore {
}
