package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.SignedInteger;

/**
 * This type represents the exponent part of a floating point number
 * Created by kfgodel on 04/08/14.
 */
public interface ExponentPart {

    /**
     * @return The character that serves as en exponent
     */
    ExponentIndicator marker();

    /**
     * @return The digits that indicate exponent size
     */
    SignedInteger exponentValue();
}
