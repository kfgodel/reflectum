package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits;

import java.util.Optional;

/**
 * This type represents a sequence of underscored hex digits
 * Created by kfgodel on 03/08/14.
 */
public interface UnderscoredHexDigits {

    /**
     * @return Starting digit
     */
    HexDigit firstDigit();

    /**
     * @return THe sequence of digits and underscores
     */
    Optional<HexDigitsAndUnderscores> middle();

    /**
     * @return Ending digit
     */
    HexDigit lastDigit();
}
