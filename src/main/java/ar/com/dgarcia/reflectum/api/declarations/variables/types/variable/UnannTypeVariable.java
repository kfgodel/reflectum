package ar.com.dgarcia.reflectum.api.declarations.variables.types.variable;

import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannReferenceType;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents a "type variable" variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannTypeVariable extends UnannReferenceType {

    /**
     * @return Identifier of the type variable
     */
    Identifier variableIdentifier();
}
