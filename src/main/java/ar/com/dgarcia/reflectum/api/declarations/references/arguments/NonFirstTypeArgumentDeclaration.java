package ar.com.dgarcia.reflectum.api.declarations.references.arguments;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents the separator and type argument used after the first one
 * Created by kfgodel on 09/08/14.
 */
public interface NonFirstTypeArgumentDeclaration {

    /**
     * @return Argument separator symbol
     */
    AsciiComma separator();

    /**
     * @return The type argument
     */
    TypeArgumentDeclaration argument();
}
