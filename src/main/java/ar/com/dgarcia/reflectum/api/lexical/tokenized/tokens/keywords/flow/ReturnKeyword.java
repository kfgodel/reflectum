package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.flow;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "return" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface ReturnKeyword extends Keyword {
}
