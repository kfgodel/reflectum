package ar.com.dgarcia.reflectum.api.declarations.references.references.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.exceptions.ExceptionTypeReference;
import ar.com.dgarcia.reflectum.api.declarations.references.custom.AnnotatedDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.references.ClassOrInterfaceTypeReference;

/**
 * This type represents the declaration of a class type
 * Created by kfgodel on 08/08/14.
 */
public interface ClassTypeReference extends ClassOrInterfaceTypeReference, AnnotatedDeclaration, ExceptionTypeReference {

}

