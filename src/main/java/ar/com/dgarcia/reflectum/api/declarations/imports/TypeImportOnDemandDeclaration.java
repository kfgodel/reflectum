package ar.com.dgarcia.reflectum.api.declarations.imports;

import ar.com.dgarcia.reflectum.api.declarations.names.PackageOrTypeName;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAsterisk;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;

/**
 * This type represents an import declaration with wildcard definition
 * Created by kfgodel on 03/08/14.
 */
public interface TypeImportOnDemandDeclaration extends ImportDeclaration {

    /**
     * @return The base type or package to import from
     */
    PackageOrTypeName importSource();

    /**
     * @return The package or type separator
     */
    AsciiDot separator();

    /**
     * @return Imported filter
     */
    AsciiAsterisk importFilter();
}
