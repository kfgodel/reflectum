package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Literal;

/**
 * This type represents a null literal value as "null"
 * Created by kfgodel on 03/08/14.
 */
public interface NullLiteral  extends Literal {

    /**
     * @return The "null" string
     */
    @Override
    String asString();
}
