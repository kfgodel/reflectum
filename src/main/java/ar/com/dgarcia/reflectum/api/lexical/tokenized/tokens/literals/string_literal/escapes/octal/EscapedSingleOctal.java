package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes.octal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits.OctalDigit;

/**
 * This type represents an escaped character expressed as a single octal value
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedSingleOctal extends OctalEscape {

    /**
     * @return First octal value
     */
    OctalDigit digit();
}
