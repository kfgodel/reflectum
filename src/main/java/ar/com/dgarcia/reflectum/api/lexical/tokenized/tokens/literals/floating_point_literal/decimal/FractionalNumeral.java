package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.DecimalFloatingPointLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;

import java.util.Optional;

/**
 * This type represents a floating point value without integer part
 * Created by kfgodel on 04/08/14.
 */
public interface FractionalNumeral extends DecimalFloatingPointLiteral {

    /**
     * @return Fractional marker
     */
    AsciiDot fractionalSeparator();

    /**
     * @return Digits of the fraction part
     */
    Digits fractionDigits();

    /**
     * @return Exponent part
     */
    Optional<ExponentPart> exponent();

    /**
     * @return Type of floating point
     */
    Optional<FloatTypeSuffix> typeSuffix();
}
