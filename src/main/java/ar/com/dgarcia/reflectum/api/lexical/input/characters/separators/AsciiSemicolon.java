package ar.com.dgarcia.reflectum.api.lexical.input.characters.separators;

import ar.com.dgarcia.reflectum.api.declarations.types.TypeDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.ClassMemberDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodBody;
import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Separator;

/**
 * This type represents the ascii ";" character
 * Created by kfgodel on 05/08/14.
 */
public interface AsciiSemicolon extends UnicodeInputCharacter, Separator, TypeDeclaration, ClassMemberDeclaration, MethodBody {

    /**
     * @return The ascii ";" character
     */
    @Override
    Character character();
}
