package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.unary;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiMinus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the decrement operator symbol "--"
 * Created by kfgodel on 05/08/14.
 */
public interface DecrementOperatorSymbol extends OperatorSymbol {

    AsciiMinus firstCharacter();
    AsciiMinus secondCharacter();
}
