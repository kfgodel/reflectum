package ar.com.dgarcia.reflectum.api.declarations.references.arguments;

import java.util.stream.Stream;

/**
 * This type represents a list of type arguments
 * Created by kfgodel on 09/08/14.
 */
public interface TypeArgumentListDeclaration {

    /**
     * @return The mandatory first arg
     */
    TypeArgumentDeclaration firstArg();

    /**
     * @return The rest of arguments
     */
    Stream<NonFirstTypeArgumentDeclaration> extraArgs();
}
