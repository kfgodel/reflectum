package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiMinus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the lambda declaration operator symbol "->"
 * Created by kfgodel on 05/08/14.
 */
public interface LambdaOperatorSymbol extends OperatorSymbol {

    /**
     * @return The argument part
     */
    AsciiMinus firstCharacter();

    /**
     * @return The body part
     */
    AsciiGt secondCharacter();
}
