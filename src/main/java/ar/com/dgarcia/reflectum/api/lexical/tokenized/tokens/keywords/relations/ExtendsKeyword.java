package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.relations;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "extends" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface ExtendsKeyword extends Keyword {
}
