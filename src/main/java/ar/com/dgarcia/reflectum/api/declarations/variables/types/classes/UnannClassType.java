package ar.com.dgarcia.reflectum.api.declarations.variables.types.classes;

/**
 * This type represents a class variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannClassType extends UnannClassOrInterfaceType, UnannInterfaceType {
}
