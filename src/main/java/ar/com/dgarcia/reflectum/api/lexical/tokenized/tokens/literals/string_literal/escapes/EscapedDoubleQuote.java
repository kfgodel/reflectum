package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiDoubleQuote;

/**
 * This type represents the escaped double quote character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedDoubleQuote extends EscapedCharacter {

    /**
     * @return The character used to indicate a double quote
     */
    AsciiDoubleQuote doubleQuoteMarker();
}
