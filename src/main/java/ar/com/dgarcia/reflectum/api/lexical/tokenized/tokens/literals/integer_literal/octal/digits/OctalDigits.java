package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits;

/**
 * This type represents a sequence of octal digits
 * Created by kfgodel on 03/08/14.
 */
public interface OctalDigits {
}
