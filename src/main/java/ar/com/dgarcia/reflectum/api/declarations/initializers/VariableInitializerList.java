package ar.com.dgarcia.reflectum.api.declarations.initializers;

import ar.com.dgarcia.reflectum.api.declarations.variables.VariableInitializer;

import java.util.stream.Stream;

/**
 * This type represents a variable initializer list
 * Created by kfgodel on 12/08/14.
 */
public interface VariableInitializerList {

    /**
     * @return First initializer of the list
     */
    VariableInitializer firstInitializer();

    /**
     * @return The rest of the initializers in the list
     */
    Stream<NonFirstVariableInitializer> extra();
}
