package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiUnderscore;

import java.util.stream.Stream;

/**
 * This type represents a sequence of underscore characters
 * Created by kfgodel on 03/08/14.
 */
public interface Underscores {

    /**
     * @return The first underscore char
     */
    AsciiUnderscore firstCharacter();

    /**
     * @return The trailing underscores
     */
    Stream<AsciiUnderscore> tailCharacters();
}
