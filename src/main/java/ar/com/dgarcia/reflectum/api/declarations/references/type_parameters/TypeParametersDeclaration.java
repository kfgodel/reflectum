package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiLt;

/**
 * This type represents a set of type parameters
 * Created by kfgodel on 11/08/14.
 */
public interface TypeParametersDeclaration {
    /**
     * @return Parameter list opening symbol
     */
    AsciiLt opening();

    /**
     * @return The list of type arguments
     */
    TypeParameterListDeclaration list();

    /**
     * @return Parameter list closing symbol
     */
    AsciiGt closing();

}
