package ar.com.dgarcia.reflectum.api.declarations.references.references;

import ar.com.dgarcia.reflectum.api.declarations.references.ReferenceTypeReference;
import ar.com.dgarcia.reflectum.api.declarations.references.custom.AnnotatedDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.exceptions.ExceptionTypeReference;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents the declaration of a type variable
 * Created by kfgodel on 08/08/14.
 */
public interface TypeVariableReference extends ReferenceTypeReference, AnnotatedDeclaration, ExceptionTypeReference {
    /**
     * @return The name of the variable
     */
    Identifier identifier();
}
