package ar.com.dgarcia.reflectum.api.declarations.references.arguments.bounds;

/**
 * This type represents the declaration of an upper bound for a type wildcard
 * Created by kfgodel on 09/08/14.
 */
public interface UpperWildcardBoundDeclaration extends WildcardBoundsDeclaration {

    /**
     * @return The "extends" word to indicate this is a upper bound
     */
    @Override
    String discriminator();
}
