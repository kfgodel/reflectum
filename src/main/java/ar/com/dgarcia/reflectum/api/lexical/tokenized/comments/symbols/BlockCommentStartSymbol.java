package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.symbols;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAsterisk;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiSlash;

/**
 * This type represents the symbol used to start a block comment
 * Created by kfgodel on 03/08/14.
 */
public interface BlockCommentStartSymbol {

    /**
     * @return the first character of two character symbol
     */
    AsciiSlash firstCharacter();
    /**
     * @return the second character of two character symbol
     */
    AsciiAsterisk secondCharacter();
}
