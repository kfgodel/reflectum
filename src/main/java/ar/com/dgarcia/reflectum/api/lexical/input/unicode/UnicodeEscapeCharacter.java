package ar.com.dgarcia.reflectum.api.lexical.input.unicode;

/**
 * This type represents the unicode scape character "\" backslash
 * Created by kfgodel on 03/08/14.
 */
public interface UnicodeEscapeCharacter extends UnicodeInputCharacter {

    /**
     * @return The backslash "\" character
     */
    @Override
    Character character();
}
