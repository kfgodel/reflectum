package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.arithmetic;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPlus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the additive operator symbol "+"
 * Created by kfgodel on 05/08/14.
 */
public interface AdditiveOperatorSymbol extends OperatorSymbol {

    AsciiPlus character();
}
