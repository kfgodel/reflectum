package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.arithmetic;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPercentage;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the remainder operator symbol "%"
 * Created by kfgodel on 05/08/14.
 */
public interface RemainderOperatorSymbol extends OperatorSymbol {
    AsciiPercentage character();
}
