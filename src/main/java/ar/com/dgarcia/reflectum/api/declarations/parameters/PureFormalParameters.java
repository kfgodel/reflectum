package ar.com.dgarcia.reflectum.api.declarations.parameters;

import java.util.stream.Stream;

/**
 * This type represents a sequence of formal parameters
 * Created by kfgodel on 12/08/14.
 */
public interface PureFormalParameters extends FormalParameters {

    /**
     * @return The first parameter of the sequence
     */
    FormalParameter firstParameter();

    /**
     * @return The optional extra parameters of the sequence
     */
    Stream<FormalParameter> rest();

}
