package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared.CommentTailStar;

/**
 * This type represents the slash "/" character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiSlash extends UnicodeInputCharacter, CommentTailStar {

    /**
     * @return The "/" character
     */
    @Override
    Character character();
}
