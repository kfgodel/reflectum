package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.lines.LineTerminator;
import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii Carriage Return character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiCr extends UnicodeInputCharacter, LineTerminator {

    /**
     * @return The "\r" character
     */
    @Override
    Character character();
}
