package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods;

import ar.com.dgarcia.reflectum.api.declarations.parameters.FormalParameterList;
import ar.com.dgarcia.reflectum.api.declarations.references.references.arrays.Dims;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftParen;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightParen;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;

/**
 * This type represents the method signature of the header
 * Created by kfgodel on 12/08/14.
 */
public interface MethodDeclarator {

    /**
     * @return Method identifier name
     */
    Identifier identifier();

    /**
     * @return Parameters opening symbol
     */
    AsciiLeftParen opening();

    /**
     * @return Parameters declaration
     */
    Optional<FormalParameterList> parameters();

    /**
     * @return Closing parameters symbol
     */
    AsciiRightParen closing();

    /**
     * @return Redundant dimensions for the return type
     */
    Optional<Dims> dimensions();
}
