package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

/**
 * This type represetns one of the floating point type suffixes
 * Created by kfgodel on 04/08/14.
 */
public interface FloatTypeSuffix {
}
