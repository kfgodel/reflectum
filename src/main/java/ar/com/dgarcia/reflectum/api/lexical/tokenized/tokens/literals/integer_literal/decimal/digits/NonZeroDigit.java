package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a numerical digit different than 0
 * Created by kfgodel on 03/08/14.
 */
public interface NonZeroDigit extends UnicodeInputCharacter, Digit {

    /**
     * @return Any of 1 2 3 4 5 6 7 8 9
     */
    @Override
    Character character();
}
