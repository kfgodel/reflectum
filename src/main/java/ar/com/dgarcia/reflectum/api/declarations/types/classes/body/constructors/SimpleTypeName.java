package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors;

/**
 * This type represents the name of a type without package
 * Created by kfgodel on 12/08/14.
 */
public interface SimpleTypeName {
}
