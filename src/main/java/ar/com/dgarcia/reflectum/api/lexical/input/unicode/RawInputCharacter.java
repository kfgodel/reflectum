package ar.com.dgarcia.reflectum.api.lexical.input.unicode;

/**
 * Created by kfgodel on 03/08/14.
 */
public interface RawInputCharacter extends UnicodeInputCharacter {

    /**
     * @return Any possible unicode character
     */
    @Override
    Character character();
}
