package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.SignedInteger;

/**
 * This type represents a binary exponent for a hex floating point numeral
 * Created by kfgodel on 04/08/14.
 */
public interface BinaryExponent {

    /**
     * @return The exponent indicator character
     */
    BinaryExponentIndicator indicator();

    /**
     * @return The exopnent value
     */
    SignedInteger value();
}
