package ar.com.dgarcia.reflectum.api.declarations.references.references;

import ar.com.dgarcia.reflectum.api.declarations.references.ReferenceTypeReference;
import ar.com.dgarcia.reflectum.api.declarations.references.references.arrays.Dims;

/**
 * This type represents the declaration of an array type
 * Created by kfgodel on 08/08/14.
 */
public interface ArrayTypeReference extends ReferenceTypeReference {

    /**
     * @return Type of the array elements
     */
    ReferenceTypeReference elementType();

    /**
     * @return Array dimension definition
     */
    Dims dimensions();
}
