package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiSlash;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the divide and assign operator symbol "/="
 * Created by kfgodel on 05/08/14.
 */
public interface DivisionAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiSlash firstCharacter();
    AsciiEqual secondCharacter();
}
