package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the unsigned right shift operator symbol ">>>"
 * Created by kfgodel on 05/08/14.
 */
public interface UnsignedRightShiftOperatorSymbol extends OperatorSymbol {
    AsciiGt firstCharacter();
    AsciiGt secondCharacter();
    AsciiGt thirdCharacter();
}
