package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciin;

/**
 * This type represents the escaped line feed character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedLineFeed extends EscapedCharacter {

    /**
     * @return The character used to mark a linefeed
     */
    Asciin linefeedMarker();
}
