package ar.com.dgarcia.reflectum.api.declarations.initializers;

import ar.com.dgarcia.reflectum.api.declarations.variables.VariableInitializer;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents the second and so initializers of a list
 * Created by kfgodel on 12/08/14.
 */
public interface NonFirstVariableInitializer {

    /**
     * @return List separator symbol
     */
    AsciiComma separator();

    /**
     * @return Variable initializer
     */
    VariableInitializer initializer();
}
