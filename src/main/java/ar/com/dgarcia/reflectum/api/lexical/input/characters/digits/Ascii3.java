package ar.com.dgarcia.reflectum.api.lexical.input.characters.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.NonZeroDigit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits.OctalDigit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes.octal.ZeroToThree;

/**
 * This type represents the ascii "3" character
 * Created by kfgodel on 04/08/14.
 */
public interface Ascii3 extends UnicodeInputCharacter, Digit, HexDigit, OctalDigit, NonZeroDigit, ZeroToThree {

    /**
     * @return The "3" character
     */
    @Override
    Character character();
}
