package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.identifiers;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a character that is a java letter or digit
 * Created by kfgodel on 03/08/14.
 */
public interface JavaLetterOrDigit extends UnicodeInputCharacter {

    /**
     * @return Any char that is a java letter or digit
     */
    @Override
    Character character();
}
