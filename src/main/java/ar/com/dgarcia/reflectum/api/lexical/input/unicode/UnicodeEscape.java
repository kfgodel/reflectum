package ar.com.dgarcia.reflectum.api.lexical.input.unicode;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;

/**
 * Created by kfgodel on 03/08/14.
 */
public interface UnicodeEscape extends UnicodeInputCharacter {

    /**
     * @return The starting scape character
     */
    UnicodeEscapeCharacter scapeCharacter();

    /**
     * @return The unicode indicator for scape sequence
     */
    UnicodeMarker marker();

    /**
     * @return A digit of the unicode sequence
     */
    HexDigit firstDigit();
    /**
     * @return A digit of the unicode sequence
     */
    HexDigit secondDigit();
    /**
     * @return A digit of the unicode sequence
     */
    HexDigit thirdDigit();
    /**
     * @return A digit of the unicode sequence
     */
    HexDigit fourthDigit();

    /**
     * @return The escaped character
     */
    @Override
    Character character();
}
