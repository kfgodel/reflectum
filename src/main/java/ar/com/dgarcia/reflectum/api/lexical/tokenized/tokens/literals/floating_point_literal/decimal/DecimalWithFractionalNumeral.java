package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.DecimalFloatingPointLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;

import java.util.Optional;

/**
 * Created by kfgodel on 04/08/14.
 */
public interface DecimalWithFractionalNumeral extends DecimalFloatingPointLiteral {

    /**
     * @return Digits that indicate the integer value
     */
    Digits integerDigits();

    /**
     * @return Marker for fractional digits
     */
    AsciiDot fractionalSeparator();

    /**
     * @return Digits for the fractional part
     */
    Optional<Digits> fractionDigits();

    /**
     * @return Exponent of the number
     */
    Optional<ExponentPart> exponent();

    /**
     * @return Suffix that indicates float or double type
     */
    Optional<FloatTypeSuffix> typeSuffix();
}
