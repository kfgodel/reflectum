package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits;

import java.util.stream.Stream;

/**
 * This type represents a sequence of digits and underscores
 * Created by kfgodel on 03/08/14.
 */
public interface DigitsAndUnderscores {

    /**
     * @return The first digit or underscore
     */
    DigitOrUnderscore firstSymbol();

    /**
     * @return Rest of the sequence
     */
    Stream<DigitOrUnderscore> tailSymbols();
}
