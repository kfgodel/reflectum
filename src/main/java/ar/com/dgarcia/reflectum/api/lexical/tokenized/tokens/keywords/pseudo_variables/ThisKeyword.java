package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.pseudo_variables;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "this" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface ThisKeyword extends Keyword {
}
