package ar.com.dgarcia.reflectum.api.declarations.references.custom;

/**
 * This type represents the range of values for a type
 * Created by kfgodel on 07/08/14.
 */
public class ValueRange<T> {

    private T min;
    private T max;

    public T getMin(){
        return min;
    }
    public T getMax(){
        return max;
    }

    public static <T> ValueRange<T> create(T min, T max){
        ValueRange<T> range = new ValueRange<T>();
        range.max = max;
        range.min = min;
        return range;
    }
}
