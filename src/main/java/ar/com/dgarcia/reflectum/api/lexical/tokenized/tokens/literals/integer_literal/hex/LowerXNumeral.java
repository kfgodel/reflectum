package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciix;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigits;

/**
 * this type represents a "0x" hex numeral
 * Created by kfgodel on 03/08/14.
 */
public interface LowerXNumeral extends HexNumeral {

    /**
     * @return The "x" character used to indicate a hex numeral
     */
    Asciix hexMarker();

    /**
     * @return The numeral digits
     */
    HexDigits digits();
}
