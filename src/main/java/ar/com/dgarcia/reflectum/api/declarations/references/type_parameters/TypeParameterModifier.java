package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters;

/**
 * This type represents the declaration of a type parameter modifier
 * Created by kfgodel on 09/08/14.
 */
public interface TypeParameterModifier {
}
