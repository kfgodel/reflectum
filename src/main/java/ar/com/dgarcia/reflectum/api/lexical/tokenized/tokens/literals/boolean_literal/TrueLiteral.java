package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.boolean_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.BooleanLiteral;

/**
 * This type represents the "true" literal value
 * Created by kfgodel on 04/08/14.
 */
public interface TrueLiteral extends BooleanLiteral {

    /**
     * @return The "true" string
     */
    @Override
    String asString();
}
