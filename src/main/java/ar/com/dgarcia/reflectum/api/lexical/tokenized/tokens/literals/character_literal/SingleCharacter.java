package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.character_literal;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a character except "'" and "\"
 * Created by kfgodel on 05/08/14.
 */
public interface SingleCharacter extends UnicodeInputCharacter {

    /**
     * @return Any character except "'" or "\"
     */
    @Override
    Character character();
}
