package ar.com.dgarcia.reflectum.api.declarations.references.arguments.bounds;

import ar.com.dgarcia.reflectum.api.declarations.references.ReferenceTypeReference;

/**
 * This type represents one of the possible type bounds for a wildcard declaration
 * Created by kfgodel on 09/08/14.
 */
public interface WildcardBoundsDeclaration {

    /**
     * @return The special word that indicates if this is upper or lower bound
     */
    String discriminator();

    /**
     * @return The type to bind to
     */
    ReferenceTypeReference boundingType();
}
