package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.AsciiX;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigits;

/**
 * This type represents a "0X" hex numeral
 * Created by kfgodel on 03/08/14.
 */
public interface CapitalXNumeral extends HexNumeral {

    /**
     * @return The "X" character used to indicate a hex numeral
     */
    AsciiX hexMarker();

    /**
     * @return The numeral digits
     */
    HexDigits digits();
}
