package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits;

/**
 * This type represents a hex digit or underscore
 * Created by kfgodel on 03/08/14.
 */
public interface HexDigitOrUnderscore {
}
