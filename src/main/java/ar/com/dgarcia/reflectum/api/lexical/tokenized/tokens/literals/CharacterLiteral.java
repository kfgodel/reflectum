package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Literal;

/**
 * This type represents a character literal value as "'a'"
 * Created by kfgodel on 03/08/14.
 */
public interface CharacterLiteral extends Literal {
}
