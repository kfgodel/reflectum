package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.unary;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPlus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the increment operator symbol "++"
 * Created by kfgodel on 05/08/14.
 */
public interface IncrementOperatorSymbol extends OperatorSymbol {

    AsciiPlus firstCharacter();
    AsciiPlus secondCharacter();
}
