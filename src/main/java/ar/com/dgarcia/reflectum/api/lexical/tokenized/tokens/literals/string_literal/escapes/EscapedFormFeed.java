package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciif;

/**
 * This type represents the escaped form feed character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedFormFeed extends EscapedCharacter {

    /**
     * @return The character to mark a form feed
     */
    Asciif formfeedMarker();
}
