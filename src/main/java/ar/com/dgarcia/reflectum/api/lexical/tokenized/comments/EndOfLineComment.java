package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments;

import ar.com.dgarcia.reflectum.api.lexical.input.lines.InputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.symbols.LineCommentSymbol;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.Comment;

import java.util.stream.Stream;

/**
 * This type represents a one line comment
 * Created by kfgodel on 03/08/14.
 */
public interface EndOfLineComment extends Comment {

    /**
     * @return The starting keyword for the comment
     */
    LineCommentSymbol startSymbol();

    /**
     * @return Contents of the line comment
     */
    Stream<InputCharacter> contents();
}
