package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents an octal digit
 * Created by kfgodel on 03/08/14.
 */
public interface OctalDigit extends UnicodeInputCharacter, OctalDigits, OctalDigitOrUnderscore {

    /**
     * @return Any of 0 1 2 3 4 5 6 7
     */
    @Override
    Character character();
}
