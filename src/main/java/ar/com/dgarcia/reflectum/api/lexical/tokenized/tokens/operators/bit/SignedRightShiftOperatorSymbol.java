package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the signed right shift operator symbol ">>"
 * Created by kfgodel on 05/08/14.
 */
public interface SignedRightShiftOperatorSymbol extends OperatorSymbol {
    AsciiGt firstCharacter();
    AsciiGt secondCharacter();
}
