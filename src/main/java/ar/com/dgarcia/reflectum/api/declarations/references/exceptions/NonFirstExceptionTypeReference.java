package ar.com.dgarcia.reflectum.api.declarations.references.exceptions;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;
import com.sun.xml.internal.ws.api.model.ExceptionType;

/**
 * This type represents the second and so excpetion type reference
 * Created by kfgodel on 12/08/14.
 */
public interface NonFirstExceptionTypeReference {

    /**
     * @return List separator symbol
     */
    AsciiComma separator();

    /**
     * @return The type reference
     */
    ExceptionTypeReference exceptionReference();
}
