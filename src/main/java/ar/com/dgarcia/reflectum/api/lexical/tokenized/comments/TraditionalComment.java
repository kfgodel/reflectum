package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.CommentTail;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.symbols.BlockCommentStartSymbol;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.Comment;

/**
 * This type represents a block comment with start and end delimiters
 * Created by kfgodel on 03/08/14.
 */
public interface TraditionalComment extends Comment {

    /**
     * @return The symbol used to start the comment
     */
    BlockCommentStartSymbol startSymbol();

    /**
     * @return The content and end of the comment
     */
    CommentTail tail();
}
