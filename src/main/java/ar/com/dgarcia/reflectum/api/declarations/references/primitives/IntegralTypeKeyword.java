package ar.com.dgarcia.reflectum.api.declarations.references.primitives;

/**
 * This type represents one of the numeric integral types
 * Created by kfgodel on 07/08/14.
 */
public interface IntegralTypeKeyword extends NumericTypeKeyword {
}
