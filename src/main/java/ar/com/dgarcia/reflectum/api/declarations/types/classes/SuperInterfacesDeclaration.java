package ar.com.dgarcia.reflectum.api.declarations.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.interfaces.InterfaceTypeListReference;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.relations.ImplementsKeyword;

/**
 * This type represents the declaration of interfaces implementation
 * Created by kfgodel on 11/08/14.
 */
public interface SuperInterfacesDeclaration {

    /**
     * @return The keyword that starts the declaration
     */
    ImplementsKeyword implementsKeyword();

    /**
     * @return The list of super interfaces
     */
    InterfaceTypeListReference interfaceList();
}
