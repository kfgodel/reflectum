package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii "%" character
 * Created by kfgodel on 05/08/14.
 */
public interface AsciiPercentage extends UnicodeInputCharacter {

    /**
     * @return The ascii "%" character
     */
    @Override
    Character character();
}
