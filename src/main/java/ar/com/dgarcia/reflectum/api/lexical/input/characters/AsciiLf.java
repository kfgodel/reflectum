package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.lines.LineTerminator;
import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the Line Feed ascii character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiLf extends UnicodeInputCharacter, LineTerminator {

    /**
     * @return The "\n" character
     */
    @Override
    public Character character();
}
