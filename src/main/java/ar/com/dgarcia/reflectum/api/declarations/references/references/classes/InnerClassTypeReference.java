package ar.com.dgarcia.reflectum.api.declarations.references.references.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.arguments.TypeArgumentsDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.custom.AnnotatedDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.references.ClassOrInterfaceTypeReference;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;

/**
 * This type represents the declaration of a inner class type
 * Created by kfgodel on 08/08/14.
 */
public interface InnerClassTypeReference extends ClassTypeReference, AnnotatedDeclaration {

    /**
     * @return The outer type
     */
    ClassOrInterfaceTypeReference declaringType();

    /**
     * @return The symbol to separate outer from inner
     */
    AsciiDot innerMarker();

    /**
     * @return The name that identifies this class
     */
    Identifier identifier();

    /**
     * @return The arguments needed for this type
     */
    Optional<TypeArgumentsDeclaration> typeArguments();

}
