package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAmpersand;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the bitwise AND operator symbol "&"
 * Created by kfgodel on 05/08/14.
 */
public interface BitwiseAndOperatorSymbol extends OperatorSymbol {
    AsciiAmpersand character();
}
