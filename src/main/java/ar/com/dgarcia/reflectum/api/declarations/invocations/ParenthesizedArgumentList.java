package ar.com.dgarcia.reflectum.api.declarations.invocations;

import ar.com.dgarcia.reflectum.api.declarations.arguments.ArgumentList;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftParen;

import java.util.Optional;

/**
 * This type represents an argument list surrounded by parenthesis
 * Created by kfgodel on 14/08/14.
 */
public interface ParenthesizedArgumentList {

    /**
     * @return Invocation argument start symbol
     */
    AsciiLeftParen opening();

    /**
     * @return List of arguments
     */
    Optional<ArgumentList> arguments();

    /**
     * @return Invocation argument end symbol
     */
    AsciiLeftParen closing();

}
