package ar.com.dgarcia.reflectum.api.declarations.parameters;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents an outer identifier declaration
 * Created by kfgodel on 12/08/14.
 */
public interface OuterIdentifier {
    /**
     * @return The identifier reference
     */
    Identifier identifier();

    /**
     * @return The nesting separator symbol
     */
    AsciiDot nestingSeparator();
}
