package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa;

/**
 * This type represents a binary exponent indicator
 * Created by kfgodel on 04/08/14.
 */
public interface BinaryExponentIndicator {
}
