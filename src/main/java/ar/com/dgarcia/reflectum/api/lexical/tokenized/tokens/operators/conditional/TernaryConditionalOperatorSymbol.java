package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.conditional;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiColon;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiQuestion;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the ternary "?:" operator symbol
 * Created by kfgodel on 05/08/14.
 */
public interface TernaryConditionalOperatorSymbol extends OperatorSymbol {

    /**
     * @return The question separator
     */
    AsciiQuestion firstCharacter();

    /**
     * @return The consequence separator
     */
    AsciiColon secondCharacter();
}
