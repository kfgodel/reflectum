package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods;

import ar.com.dgarcia.reflectum.api.declarations.references.annotations.AnnotationDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.TypeParametersDeclaration;

import java.util.stream.Stream;

/**
 * This type represents an annotated method header
 * Created by kfgodel on 12/08/14.
 */
public interface ParametrizedMethodHeader extends MethodHeader {

    /**
     * @return Type parameters of the method
     */
    TypeParametersDeclaration methodParameters();

    /**
     * @return Modifying annotations
     */
    Stream<AnnotationDeclaration> annotations();

    /**
     * @return Method header
     */
    UnparametrizedMethodHeader basicHeader();
}
