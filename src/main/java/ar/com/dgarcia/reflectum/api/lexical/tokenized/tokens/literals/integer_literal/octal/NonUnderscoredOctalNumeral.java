package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits.OctalDigits;

/**
 * This type represents an octal numeral without underscores
 * Created by kfgodel on 03/08/14.
 */
public interface NonUnderscoredOctalNumeral extends OctalNumeral {

    /**
     * @return The octal value
     */
    OctalDigits digits();
}
