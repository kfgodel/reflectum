package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.boolean_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.BooleanLiteral;

/**
 * This type represents the "false" literal
 * Created by kfgodel on 04/08/14.
 */
public interface FalseLiteral extends BooleanLiteral {

    /**
     * @return The "false" string
     */
    @Override
    String asString();
}
