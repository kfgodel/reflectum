package ar.com.dgarcia.reflectum.api.declarations.packages;

/**
 * This type represents a modifier for a package declaration
 * Created by kfgodel on 11/08/14.
 */
public interface PackageModifier {
}
