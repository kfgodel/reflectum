package ar.com.dgarcia.reflectum.api.declarations.variables.types.arrays;

import ar.com.dgarcia.reflectum.api.declarations.references.references.arrays.Dims;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.variable.UnannTypeVariable;

/**
 * This type represents a "type variable" array variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannVariableArrayType extends UnannArrayType {
    /**
     * @return Type for the elements of the array
     */
    UnannTypeVariable elementType();

    /**
     * @return Array dimensions
     */
    Dims dimensions();
}
