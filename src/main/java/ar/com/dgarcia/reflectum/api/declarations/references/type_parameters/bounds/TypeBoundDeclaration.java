package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.bounds;

/**
 * This type represents a type parameter bound declaration
 * Created by kfgodel on 09/08/14.
 */
public interface TypeBoundDeclaration {

    /**
     * @return reserved word "extends" that marks the start of the type
     */
    String prefix();
}
