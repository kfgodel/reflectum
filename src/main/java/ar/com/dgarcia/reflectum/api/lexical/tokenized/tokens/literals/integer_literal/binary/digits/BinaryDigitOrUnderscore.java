package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits;

/**
 * This type represents a binary digit or an underscore
 * Created by kfgodel on 04/08/14.
 */
public interface BinaryDigitOrUnderscore {
}
