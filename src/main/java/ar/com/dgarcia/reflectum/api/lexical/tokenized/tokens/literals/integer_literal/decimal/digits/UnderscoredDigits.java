package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits;

import java.util.Optional;

/**
 * This type represents and underscored sequence of digits
 * Created by kfgodel on 03/08/14.
 */
public interface UnderscoredDigits extends Digits {

    /**
     * @return The first digit
     */
    Digit firstDigit();

    /**
     * @return The digit sequence with underscores
     */
    Optional<DigitsAndUnderscores> middle();

    /**
     * @return The last digit
     */
    Digit lastDigit();
}
