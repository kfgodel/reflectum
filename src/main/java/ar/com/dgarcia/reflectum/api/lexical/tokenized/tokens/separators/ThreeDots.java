package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.separators;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Separator;

/**
 * This type represents the three dots symbol
 * Created by kfgodel on 05/08/14.
 */
public interface ThreeDots extends Separator {
    /**
     * @return First of the sequence
     */
    AsciiDot firstDot();

    /**
     * @return Second in the sequence
     */
    AsciiDot secondDot();

    /**
     * @return Third and last in the sequence
     */
    AsciiDot thirdDot();
}
