package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.DecimalFloatingPointLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;

import java.util.Optional;

/**
 * This type represents one of the decimal floating point literals
 * Created by kfgodel on 04/08/14.
 */
public interface DecimalWithExponentNumeral extends DecimalFloatingPointLiteral {

    /**
     * @return Integer digits
     */
    Digits integerDigits();

    /**
     * @return The exponent part
     */
    ExponentPart exponent();

    /**
     * @return The floating point type
     */
    Optional<FloatTypeSuffix> typeSuffix();
}
