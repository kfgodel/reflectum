package ar.com.dgarcia.reflectum.api.lexical.input.characters;

/**
 * This types is an artificial supertype for symbols that can be represented by one character
 * Created by kfgodel on 03/08/14.
 */
public interface CharacterSymbol {

    /**
     * @return This symbols as a character
     */
    Character character();
}
