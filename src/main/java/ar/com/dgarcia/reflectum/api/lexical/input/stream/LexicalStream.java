package ar.com.dgarcia.reflectum.api.lexical.input.stream;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

import java.util.stream.Stream;

/**
 * This type represents the unicode unit for a parser to interpret a compilation unit
 * Created by kfgodel on 03/08/14.
 */
public interface LexicalStream {

    /**
     * @return The unicode stream of character to be interpreted as a java construct
     */
    Stream<UnicodeInputCharacter> stream();
}
