/**
 * This package tries to represent the language element of Java 8 based on: http://docs.oracle.com/javase/specs/jls/se8/html/index.html
 * Created by kfgodel on 03/08/14.
 */
package ar.com.dgarcia.reflectum.api;