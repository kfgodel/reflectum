package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.IntegerLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.HexNumeral;

import java.util.Optional;

/**
 * This type represents a base 16 integer value
 * Created by kfgodel on 03/08/14.
 */
public interface HexIntegerLiteral extends IntegerLiteral {

    /**
     * @return The numeral literal
     */
    HexNumeral numeral();

    /**
     * @return The type of value
     */
    Optional<IntegerTypeSuffix> suffix();

}
