package ar.com.dgarcia.reflectum.api.declarations.names;

/**
 * This type represents the name of an ambiguous expression
 * Created by kfgodel on 09/08/14.
 */
public interface AmbiguousName {
}
