package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.Underscores;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.NonZeroDigit;

/**
 * This type represents a decimal numeral with underscores
 * Created by kfgodel on 03/08/14.
 */
public interface UnderscoredDecimalNumeral extends DecimalNumeral {

    /**
     * @return Starting digit
     */
    NonZeroDigit firstDigit();

    /**
     * @return separators underscores
     */
    Underscores underscores();

    /**
     * @return rest of digits
     */
    Digits tailDigits();
}
