package ar.com.dgarcia.reflectum.api.declarations.references.exceptions;

import java.util.stream.Stream;

/**
 * This type represents a list of exception declarations
 * Created by kfgodel on 12/08/14.
 */
public interface ExceptionTypeListReference {
    /**
     * @return The first exception reference
     */
    ExceptionTypeReference firstType();

    /**
     * @return The rest of the references
     */
    Stream<NonFirstExceptionTypeReference> extra();
}
