package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.relational;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiLt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the "<" operator symbol
 * Created by kfgodel on 05/08/14.
 */
public interface LtOperatorSymbol extends OperatorSymbol {

    /**
     * @return The character that represents this operator
     */
    AsciiLt character();
}
