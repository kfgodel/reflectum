package ar.com.dgarcia.reflectum.api.declarations.invocations;

import ar.com.dgarcia.reflectum.api.declarations.arguments.ArgumentList;
import ar.com.dgarcia.reflectum.api.declarations.references.arguments.TypeArgumentsDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftParen;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiSemicolon;

import java.util.Optional;

/**
 * This type represents the explicit constructor invocation
 * Created by kfgodel on 12/08/14.
 */
public interface ExplicitConstructorInvocation {

    /**
     * @return The arguments used for the invocation
     */
    Optional<TypeArgumentsDeclaration> typeArguments();

    /**
     * @return Constructor arguments
     */
    ParenthesizedArgumentList arguments();

    /**
     * @return Statement end symbol
     */
    AsciiSemicolon ending();
}
