package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.types;

import ar.com.dgarcia.reflectum.api.declarations.references.primitives.IntegralTypeKeyword;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * Created by kfgodel on 03/08/14.
 */
public interface ByteKeyword extends Keyword, IntegralTypeKeyword{
}
