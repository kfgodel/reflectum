package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.types;

import ar.com.dgarcia.reflectum.api.declarations.references.primitives.FloatingPointTypeKeyword;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "float" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface FloatKeyword extends Keyword, FloatingPointTypeKeyword {
}
