package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.ClassMemberDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodBody;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodHeader;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodModifier;

import java.util.stream.Stream;

/**
 * This type represents the declaration of a class method
 * Created by kfgodel on 11/08/14.
 */
public interface MethodDeclaration extends ClassMemberDeclaration {
    /**
     * @return Modifiers of the method declaration
     */
    Stream<MethodModifier> modifiers();

    /**
     * @return Signature declaration
     */
    MethodHeader header();

    /**
     * @return Body definition
     */
    MethodBody body();
}
