package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodModifier;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "native" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface NativeKeyword extends Keyword, MethodModifier {
}
