package ar.com.dgarcia.reflectum.api.declarations.parameters;

import java.util.stream.Stream;

/**
 * This type represents a receiver and formal parameters sequence
 * Created by kfgodel on 12/08/14.
 */
public interface ReceiverAndFormalParameters extends FormalParameters{

    /**
     * @return The first parameter of the sequence
     */
    ReceiverParameter firstParameter();

    /**
     * @return The optional extra parameters of the sequence
     */
    Stream<FormalParameter> rest();
}
