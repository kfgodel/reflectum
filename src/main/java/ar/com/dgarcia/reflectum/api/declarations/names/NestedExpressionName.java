package ar.com.dgarcia.reflectum.api.declarations.names;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents the name of a nested expression
 * Created by kfgodel on 09/08/14.
 */
public interface NestedExpressionName extends ExpressionName, NestedName {
    /**
     * @return The name of the parent expression
     */
    AmbiguousName parentName();

}
