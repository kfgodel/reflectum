package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.types;

import ar.com.dgarcia.reflectum.api.declarations.references.primitives.PrimitiveTypeKeyword;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannPrimitiveType;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "boolean" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface BooleanKeyword extends Keyword, PrimitiveTypeKeyword, UnannPrimitiveType {
}
