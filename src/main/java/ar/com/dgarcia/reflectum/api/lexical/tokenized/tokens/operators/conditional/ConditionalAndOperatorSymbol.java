package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.conditional;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAmpersand;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the conditional AND operator symbol "&&"
 * Created by kfgodel on 05/08/14.
 */
public interface ConditionalAndOperatorSymbol extends OperatorSymbol {

    AsciiAmpersand firstCharacter();
    AsciiAmpersand secondCharacter();
}
