package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAsterisk;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the multiply and assign operator symbol "*="
 * Created by kfgodel on 05/08/14.
 */
public interface MultiplicationAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiAsterisk firstCharacter();
    AsciiEqual secondCharacter();
}
