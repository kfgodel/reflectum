package ar.com.dgarcia.reflectum.api.declarations.parameters;

import ar.com.dgarcia.reflectum.api.declarations.references.annotations.AnnotationDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannType;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.pseudo_variables.ThisKeyword;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents a receiver parameter
 * Created by kfgodel on 12/08/14.
 */
public interface ReceiverParameter {

    /**
     * @return Optional annotations for the type
     */
    Stream<AnnotationDeclaration> annotations();

    /**
     * @return The parameter type
     */
    UnannType parameterType();

    /**
     * @return An outer reference
     */
    Optional<OuterIdentifier> outerIdentifier();

    /**
     * @return The this keyword
     */
    ThisKeyword thisKeyword();
}
