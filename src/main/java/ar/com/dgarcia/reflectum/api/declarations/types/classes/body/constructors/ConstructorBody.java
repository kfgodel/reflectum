package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors;

import ar.com.dgarcia.reflectum.api.declarations.invocations.ExplicitConstructorInvocation;
import ar.com.dgarcia.reflectum.api.declarations.statements.BlockStatements;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftCurly;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightCurly;

import java.util.Optional;

/**
 * This type represents the body of a constructor method
 * Created by kfgodel on 12/08/14.
 */
public interface ConstructorBody {

    /**
     * @return The opening body symbol
     */
    AsciiLeftCurly opening();

    /**
     * @return The explicit parent constructor invocation
     */
    Optional<ExplicitConstructorInvocation> constructorInvocation();

    /**
     * @return The set of statemets that form the constructor
     */
    Optional<BlockStatements> constructorStatemetns();

    /**
     * @return the body closing symbol
     */
    AsciiRightCurly closing();
}
