package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa.BinaryExponentIndicator;

/**
 * This type represents the ascii "P" character
 * Created by kfgodel on 04/08/14.
 */
public interface AsciiP extends UnicodeInputCharacter, BinaryExponentIndicator {

    /**
     * @return The "P" character
     */
    @Override
    Character character();
}
