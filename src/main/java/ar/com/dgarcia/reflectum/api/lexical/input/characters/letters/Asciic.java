package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;

/**
 * This type represents the ascii "c" character
 * Created by kfgodel on 04/08/14.
 */
public interface Asciic extends UnicodeInputCharacter, HexDigit {

    /**
     * @return The "c" character
     */
    @Override
    Character character();
}
