package ar.com.dgarcia.reflectum.api.declarations.types.classes.enums;

import ar.com.dgarcia.reflectum.api.declarations.arguments.ArgumentList;
import ar.com.dgarcia.reflectum.api.declarations.invocations.ParenthesizedArgumentList;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.ClassBody;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents a enum constant declaration
 * Created by kfgodel on 14/08/14.
 */
public interface EnumConstant {

    /**
     * @return Enum modifiers
     */
    Stream<EnumConstantModifier> modifiers();

    /**
     * @return Enum id
     */
    Identifier identifier();

    /**
     * @return Arguments for argument constructor
     */
    Optional<ParenthesizedArgumentList> arguments();

    /**
     * @return The enum body
     */
    Optional<ClassBody> body();
}
