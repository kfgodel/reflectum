package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.CommentTail;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAsterisk;

/**
 * This type represents a comment tail that starts with an asterisk
 * Created by kfgodel on 03/08/14.
 */
public interface StarStartedTail extends CommentTail {

    /**
     * @return The starting character
     */
    AsciiAsterisk startCharacter();

    /**
     * @return The contents and end of a comment star
     */
    CommentTailStar startTail();
}
