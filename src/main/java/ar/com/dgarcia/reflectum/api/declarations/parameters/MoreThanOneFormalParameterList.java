package ar.com.dgarcia.reflectum.api.declarations.parameters;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents a method parameter list with more than one parameter
 * Created by kfgodel on 12/08/14.
 */
public interface MoreThanOneFormalParameterList extends FormalParameterList {

    /**
     * @return The list of first parameters
     */
    FormalParameters firstParameters();

    /**
     * @return The list separator symbol
     */
    AsciiComma separator();

    /**
     * @return The last parameter
     */
    LastFormalParameter lastParameter();
}
