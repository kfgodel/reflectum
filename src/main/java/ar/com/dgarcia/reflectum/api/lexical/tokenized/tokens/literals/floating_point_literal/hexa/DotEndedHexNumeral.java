package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.HexNumeral;

import java.util.Optional;

/**
 * This type represents one of the hex significand forms
 * Created by kfgodel on 04/08/14.
 */
public interface DotEndedHexNumeral extends HexSignificand{

    /**
     * @return The hex integer number
     */
    HexNumeral numeral();

    /**
     * @return The fraction marker
     */
    Optional<AsciiDot> fractionalSeparator();
}
