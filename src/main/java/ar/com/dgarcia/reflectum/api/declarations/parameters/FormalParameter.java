package ar.com.dgarcia.reflectum.api.declarations.parameters;

import ar.com.dgarcia.reflectum.api.declarations.variables.VariableDeclaratorId;
import ar.com.dgarcia.reflectum.api.declarations.variables.VariableModifier;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannType;

import java.util.stream.Stream;

/**
 * This type represents a formal parameter of a method
 * Created by kfgodel on 12/08/14.
 */
public interface FormalParameter extends LastFormalParameter {

    /**
     * @return The optional parameter modifiers
     */
    Stream<VariableModifier> modifiers();

    /**
     * @return The type of this parameter
     */
    UnannType parameterType();

    /**
     * @return The parameter identification
     */
    VariableDeclaratorId declaratorId();
}
