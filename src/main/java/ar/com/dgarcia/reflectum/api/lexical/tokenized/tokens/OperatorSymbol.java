package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.Token;

/**
 * This type represents one of the language operators
 * Created by kfgodel on 03/08/14.
 */
public interface OperatorSymbol extends Token {
}
