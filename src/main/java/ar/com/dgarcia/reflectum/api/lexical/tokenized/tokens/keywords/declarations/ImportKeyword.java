package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.declarations;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "imports" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface ImportKeyword extends Keyword {
}
