package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiCircumflex;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the bitwise exclusive OR operator symbol "^"
 * Created by kfgodel on 05/08/14.
 */
public interface BitwiseExclusiveOrOperatorSymbol extends OperatorSymbol {
    AsciiCircumflex character();
}
