package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiBackslash;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.character_literal.EscapeSequence;

/**
 * This type is an artificial supertype for characters escped with a backslash
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedCharacter extends EscapeSequence{

    /**
     * @return The escape character
     */
    AsciiBackslash escape();
}
