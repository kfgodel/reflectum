package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens;

import ar.com.dgarcia.reflectum.api.declarations.names.*;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors.SimpleTypeName;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.Token;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.identifiers.IdentifierChars;

/**
 * This type represents an identifier token for a language element
 * Created by kfgodel on 03/08/14.
 */
public interface Identifier extends Token, PackageName, TypeName, PackageOrTypeName, ExpressionName, MethodName, AmbiguousName, SimpleTypeName {

    /**
     * @return The sequence of characters that form this identifier. The sequence is different from any keyword, null literal, or boolean literal
     */
    IdentifierChars chars();
}
