package ar.com.dgarcia.reflectum.api.declarations.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.enums.EnumBody;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.types.EnumKeyword;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents the declaration of an enum type
 * Created by kfgodel on 11/08/14.
 */
public interface EnumDeclaration {

    /**
     * @return Enum modifiers
     */
    Stream<ClassModifier> modifiers();

    /**
     * @return The enum declaration keyword
     */
    EnumKeyword enumKeyword();

    /**
     * @return The enum identifier
     */
    Identifier enumIdentifier();

    /**
     * @return The super interfaces declaration
     */
    Optional<SuperInterfacesDeclaration> superInterfaces();

    /**
     * @return The body of the enum
     */
    EnumBody enumBody();
}
