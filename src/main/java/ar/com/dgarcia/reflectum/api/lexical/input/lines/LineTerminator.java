package ar.com.dgarcia.reflectum.api.lexical.input.lines;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.NotStar;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared.NotStarNotSlash;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.WhiteSpace;

/**
 * This type represents the end of a line of unicode character
 * Created by kfgodel on 03/08/14.
 */
public interface LineTerminator extends WhiteSpace, NotStar, NotStarNotSlash {
}
