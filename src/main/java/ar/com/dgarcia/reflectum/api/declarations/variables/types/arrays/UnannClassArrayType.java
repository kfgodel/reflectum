package ar.com.dgarcia.reflectum.api.declarations.variables.types.arrays;

import ar.com.dgarcia.reflectum.api.declarations.references.references.arrays.Dims;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.classes.UnannClassOrInterfaceType;

/**
 * This type represents a class array variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannClassArrayType extends UnannArrayType {
    /**
     * @return Type for the array elements
     */
    UnannClassOrInterfaceType elementType();

    /**
     * @return Dimensions of the array
     */
    Dims dimensions();
}
