package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPlus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the increment and assignment operator symbol "+="
 * Created by kfgodel on 05/08/14.
 */
public interface IncrementAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiPlus firstCharacter();
    AsciiEqual secondCharacter();
}
