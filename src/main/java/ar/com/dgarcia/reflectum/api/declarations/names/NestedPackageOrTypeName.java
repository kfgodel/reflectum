package ar.com.dgarcia.reflectum.api.declarations.names;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents a name that is nested in packages or types
 * Created by kfgodel on 09/08/14.
 */
public interface NestedPackageOrTypeName extends PackageOrTypeName, NestedName {

    /**
     * @return The name of the parent package or outer type
     */
    PackageOrTypeName parentName();

}
