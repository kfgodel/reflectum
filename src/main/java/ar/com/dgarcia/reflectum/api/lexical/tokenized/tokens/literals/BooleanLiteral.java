package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Literal;

/**
 * This type represents a boolean literal value as "true"
 * Created by kfgodel on 03/08/14.
 */
public interface BooleanLiteral extends Literal {
}
