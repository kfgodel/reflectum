package ar.com.dgarcia.reflectum.api.declarations.expressions;

/**
 * This type represents the second an so expressions of a list
 * Created by kfgodel on 12/08/14.
 */
public interface NonFirstExpression {
}
