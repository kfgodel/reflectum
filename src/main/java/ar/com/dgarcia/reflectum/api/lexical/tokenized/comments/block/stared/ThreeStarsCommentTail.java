package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAsterisk;

/**
 * This type represents the contents of a comment that starts with 3 stars
 * Created by kfgodel on 03/08/14.
 */
public interface ThreeStarsCommentTail extends CommentTailStar {

    /**
     * @return The third star
     */
    AsciiAsterisk startCharacter();

    /**
     * @return Comment content
     */
    CommentTailStar tail();
}
