package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiQuote;

/**
 * This type represents the escaped single quote character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedSingleQuote extends EscapedCharacter {

    /**
     * @return The character used to indicate a quote
     */
    AsciiQuote quoteMarker();
}
