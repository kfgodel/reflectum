package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the unsigned right shift and assign operator symbol ">>>="
 * Created by kfgodel on 05/08/14.
 */
public interface UnsignedRightShiftAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiGt firstCharacter();
    AsciiGt secondCharacter();
    AsciiEqual thirdCharacter();
}