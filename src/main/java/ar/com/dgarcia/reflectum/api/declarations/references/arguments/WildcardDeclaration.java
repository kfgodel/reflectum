package ar.com.dgarcia.reflectum.api.declarations.references.arguments;

import ar.com.dgarcia.reflectum.api.declarations.references.arguments.bounds.WildcardBoundsDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.custom.AnnotatedDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiQuestion;

import java.util.Optional;

/**
 * This type represents the declaration of a wildcard
 * Created by kfgodel on 09/08/14.
 */
public interface WildcardDeclaration extends TypeArgumentDeclaration, AnnotatedDeclaration {
    /**
     * @return The symbol used to indicate the wildcard type
     */
    AsciiQuestion wildcardSymbol();

    /**
     * @return A possible type bound
     */
    Optional<WildcardBoundsDeclaration> bounds();
}
