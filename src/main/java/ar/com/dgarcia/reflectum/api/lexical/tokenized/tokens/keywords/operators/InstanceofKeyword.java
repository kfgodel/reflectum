package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.operators;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "instanceof" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface InstanceofKeyword extends Keyword {
}
