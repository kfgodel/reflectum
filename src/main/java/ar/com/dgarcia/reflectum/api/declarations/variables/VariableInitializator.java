package ar.com.dgarcia.reflectum.api.declarations.variables;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;

/**
 * This type represents a variable initialization declaration
 * Created by kfgodel on 12/08/14.
 */
public interface VariableInitializator {
    /**
     * @return Assignment symbol
     */
    AsciiEqual assignment();

    /**
     * @return Initializer declaration
     */
    VariableInitializer initializer();
}
