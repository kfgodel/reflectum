package ar.com.dgarcia.reflectum.api.lexical.tokenized.elements;

/**
 * This type represents a tokenized token which has language semantics
 * Created by kfgodel on 03/08/14.
 */
public interface Token extends InputElement {
}
