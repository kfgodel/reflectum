package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.arithmetic;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAsterisk;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents a multiplication operator symbol "*"
 * Created by kfgodel on 05/08/14.
 */
public interface MultiplicationOperatorSymbol extends OperatorSymbol {
    AsciiAsterisk character();
}
