package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.relational;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the ">" operator symbol
 * Created by kfgodel on 05/08/14.
 */
public interface GtOperatorSymbol extends OperatorSymbol {

    /**
     * @return Tha character that represents this operator
     */
    AsciiGt character();
}
