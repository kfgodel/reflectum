package ar.com.dgarcia.reflectum.api.declarations.references;

import ar.com.dgarcia.reflectum.api.declarations.references.arguments.TypeArgumentDeclaration;

/**
 * This type represents one of the object types
 * Created by kfgodel on 06/08/14.
 */
public interface ReferenceTypeReference extends TypeReference, TypeArgumentDeclaration {
}
