package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.DecimalFloatingPointLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;

import java.util.Optional;

/**
 * This type represents one of the floating point literals
 * Created by kfgodel on 04/08/14.
 */
public interface DecimalWithSuffixNumeral extends DecimalFloatingPointLiteral {

    /**
     * @return The integer digits
     */
    Digits integerDigits();

    /**
     * @return Exponent part
     */
    Optional<ExponentPart> exponent();

    /**
     * @return Type suffix
     */
    FloatTypeSuffix typeSuffix();
}
