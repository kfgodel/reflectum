package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;

import java.util.Optional;

/**
 * This type represents a signed integer value
 * Created by kfgodel on 04/08/14.
 */
public interface SignedInteger {

    /**
     * @return The positive or negative sign character
     */
    Optional<Sign> sign();

    /**
     * @return The integer digits
     */
    Digits digits();
}
