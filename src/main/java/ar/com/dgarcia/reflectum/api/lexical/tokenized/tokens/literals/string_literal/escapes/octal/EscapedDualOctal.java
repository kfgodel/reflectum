package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes.octal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits.OctalDigit;

/**
 * This type represents an escaped character expressed as two octal values
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedDualOctal extends OctalEscape {

    /**
     * @return First octal value
     */
    OctalDigit firstDigit();

    /**
     * @return secon octal value
     */
    OctalDigit secondDigit();
}
