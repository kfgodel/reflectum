package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.digits.Ascii0;

/**
 * This type represents a hex numeral literal
 * Created by kfgodel on 03/08/14.
 */
public interface HexNumeral {

    /**
     * @return Starting escape digit
     */
    Ascii0 escapeDigit();
}
