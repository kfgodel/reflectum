package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits.BinaryDigitOrUnderscore;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.DigitOrUnderscore;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigitOrUnderscore;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits.OctalDigitOrUnderscore;

/**
 * This type represents the ascii "_" underscore character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiUnderscore extends UnicodeInputCharacter, DigitOrUnderscore, HexDigitOrUnderscore, OctalDigitOrUnderscore, BinaryDigitOrUnderscore {

    /**
     * @return The "_" character
     */
    @Override
    Character character();
}
