package ar.com.dgarcia.reflectum.api.declarations.variables.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.arguments.TypeArgumentsDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;

/**
 * This type represents a non nested class variable type
 * Created by kfgodel on 12/08/14.
 */
public interface SimpleUnannClassType extends UnannClassType {

    /**
     * @return Class type identifier
     */
    Identifier classIdentifier();

    /**
     * @return Type arguments for the class
     */
    Optional<TypeArgumentsDeclaration> arguments();
}
