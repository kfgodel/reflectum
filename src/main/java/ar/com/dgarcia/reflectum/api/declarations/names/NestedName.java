package ar.com.dgarcia.reflectum.api.declarations.names;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type is an artificial supertype for names that are nested and defined as childs of a parent name
 * Created by kfgodel on 09/08/14.
 */
public interface NestedName {
    /**
     * @return The separator symbol
     */
    AsciiDot separator();

    /**
     * @return The name of this child relative to its parent
     */
    Identifier childName();

}
