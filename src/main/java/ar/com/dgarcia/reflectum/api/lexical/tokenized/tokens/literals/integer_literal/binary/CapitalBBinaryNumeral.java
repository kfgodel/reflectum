package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.AsciiB;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits.BinaryDigits;

/**
 * This type represents an binary numeral with "B"
 * Created by kfgodel on 04/08/14.
 */
public interface CapitalBBinaryNumeral extends BinaryNumeral {
    /**
     * @return The binary marker indicator
     */
    AsciiB binaryMarker();

    /**
     * @return The sequence of binary digits
     */
    BinaryDigits digits();
}
