package ar.com.dgarcia.reflectum.api.declarations.variables.types.arrays;

import ar.com.dgarcia.reflectum.api.declarations.references.references.arrays.Dims;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannPrimitiveType;

/**
 * This type represents a primitive array variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannPrimitiveArrayType extends UnannArrayType {
    /**
     * @return Type for the elements of the array
     */
    UnannPrimitiveType elementType();

    /**
     * @return the array dimensions
     */
    Dims dimension();
}
