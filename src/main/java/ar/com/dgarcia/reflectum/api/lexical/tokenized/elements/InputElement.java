package ar.com.dgarcia.reflectum.api.lexical.tokenized.elements;

/**
 * This type represents a tokenized input element
 * Created by kfgodel on 03/08/14.
 */
public interface InputElement {

    /**
     * @return The string representation of this parsed element
     */
    String asString();
}
