package ar.com.dgarcia.reflectum.api.declarations.packages;

import ar.com.dgarcia.reflectum.api.declarations.names.PackageName;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.declarations.PackageKeyword;

import java.util.stream.Stream;

/**
 * This type represents the header declaration of a package for a java compilation unit
 * Created by kfgodel on 03/08/14.
 */
public interface PackageDeclaration{

    /**
     * @return The set of package modifiers
     */
    Stream<PackageModifier> modifiers();

    /**
     * @return The keyword for a package declaration: "package"
     */
    PackageKeyword keyword();

    /**
     * @return The complete package name
     */
    PackageName name();


}
