package ar.com.dgarcia.reflectum.api.declarations.references.references.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.references.ClassOrInterfaceTypeReference;

/**
 * This type represents the declaration of an interface type
 * Created by kfgodel on 08/08/14.
 */
public interface InterfaceTypeReference extends ClassOrInterfaceTypeReference, ClassTypeReference {
}
