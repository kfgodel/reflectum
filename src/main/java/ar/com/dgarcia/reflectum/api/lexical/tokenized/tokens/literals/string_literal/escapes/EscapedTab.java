package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciit;

/**
 * This type represents the escaped tab character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedTab extends EscapedCharacter {

    /**
     * @return The horizontal tab marker
     */
    Asciit tabMarker();
}
