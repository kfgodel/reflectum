package ar.com.dgarcia.reflectum.api.lexical.input.characters.separators;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Separator;

/**
 * This type represents the ascii "." character
 * Created by kfgodel on 04/08/14.
 */
public interface AsciiDot extends UnicodeInputCharacter, Separator {

    /**
     * @return The ascii "." character
     */
    @Override
    Character character();
}
