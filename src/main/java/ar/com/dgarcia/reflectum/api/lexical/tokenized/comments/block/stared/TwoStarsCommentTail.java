package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.CommentTail;

/**
 * This type represents the contents of a two "*" stars comment
 * Created by kfgodel on 03/08/14.
 */
public interface TwoStarsCommentTail extends CommentTailStar {

    /**
     * @return The starting char that is not a "/" or a "*"
     */
    NotStarNotSlash startCharacter();

    /**
     * @return Resto of the commment
     */
    CommentTail tail();
}
