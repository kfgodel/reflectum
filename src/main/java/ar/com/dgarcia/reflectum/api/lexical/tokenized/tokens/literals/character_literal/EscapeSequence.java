package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.character_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.StringCharacter;

/**
 * This type represents a character escaped sequence
 * Created by kfgodel on 05/08/14.
 */
public interface EscapeSequence extends StringCharacter {
}
