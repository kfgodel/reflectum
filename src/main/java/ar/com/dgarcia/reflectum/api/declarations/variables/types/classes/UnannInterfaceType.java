package ar.com.dgarcia.reflectum.api.declarations.variables.types.classes;

/**
 * This type represents a interface variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannInterfaceType extends UnannClassOrInterfaceType {
}
