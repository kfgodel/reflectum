package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared;

import ar.com.dgarcia.reflectum.api.lexical.input.lines.InputCharacter;

/**
 * This type represents an input character that is not a "*" or a "/"
 * Created by kfgodel on 03/08/14.
 */
public interface NotStarNotSlashInputCharacter extends InputCharacter, NotStarNotSlash {

    /**
     * @return Any char except "*", "/", CR or LF
     */
    @Override
    Character character();
}
