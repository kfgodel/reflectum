package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Literal;

/**
 * This type represents an integer literal value as "1"
 * Created by kfgodel on 03/08/14.
 */
public interface IntegerLiteral extends Literal {
}
