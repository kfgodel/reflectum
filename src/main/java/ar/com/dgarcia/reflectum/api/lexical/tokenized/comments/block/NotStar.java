package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block;

/**
 * This type reresents a character that is not a "*"
 * Created by kfgodel on 03/08/14.
 */
public interface NotStar {
}
