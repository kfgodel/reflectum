package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.flow;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "switch" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface SwitchKeyword extends Keyword {
}
