package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.ClassMemberDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.fields.FieldModifier;
import ar.com.dgarcia.reflectum.api.declarations.variables.VariableDeclaratorList;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannType;

import java.util.stream.Stream;

/**
 * This type represents the declaration of a class field
 * Created by kfgodel on 11/08/14.
 */
public interface FieldDeclaration extends ClassMemberDeclaration{

    /**
     * @return Type modifiers
     */
    Stream<FieldModifier> modifiers();

    /**
     * @return Variable type
     */
    UnannType type();

    /**
     * @return Variable declarations
     */
    VariableDeclaratorList variableDeclarations();
}
