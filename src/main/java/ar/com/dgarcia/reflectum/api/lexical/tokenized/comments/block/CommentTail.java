package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block;

/**
 * This type represents the content and end of a block comment
 * Created by kfgodel on 03/08/14.
 */
public interface CommentTail {
}
