package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters;

import ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.bounds.TypeBoundDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.stream.Stream;

/**
 * This type represents the declaration of a type parameter
 * Created by kfgodel on 09/08/14.
 */
public interface TypeParameterDeclaration {

    /**
     * @return The optional parameter modifiers
     */
    Stream<TypeParameterModifier> modifiers();

    /**
     * @return The parameter identifier
     */
    Identifier identifier();

    /**
     * @return bound to restrict the arguments
     */
    TypeBoundDeclaration parameterBound();
}
