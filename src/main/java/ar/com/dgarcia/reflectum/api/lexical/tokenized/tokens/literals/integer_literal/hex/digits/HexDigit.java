package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents one of the possible char values for a hex digit
 * Created by kfgodel on 03/08/14.
 */
public interface HexDigit extends UnicodeInputCharacter, HexDigits, HexDigitOrUnderscore {

    /**
     * @return One of the possible: 0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F
     */
    @Override
    Character character();
}
