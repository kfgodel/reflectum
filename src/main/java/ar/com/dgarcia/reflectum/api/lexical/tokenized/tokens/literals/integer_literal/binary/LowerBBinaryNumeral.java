package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciib;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits.BinaryDigits;

/**
 * This type represents a binary numeral with "b"
 * Created by kfgodel on 04/08/14.
 */
public interface LowerBBinaryNumeral extends BinaryNumeral {
    /**
     * @return The binary marker indicator
     */
    Asciib binaryMarker();

    /**
     * @return The sequence of binary digits
     */
    BinaryDigits digits();
}
