package ar.com.dgarcia.reflectum.api.declarations.variables.types;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.ResultDeclaration;

/**
 * This type represents a type of variable declaration
 * Created by kfgodel on 12/08/14.
 */
public interface UnannType extends ResultDeclaration {
}
