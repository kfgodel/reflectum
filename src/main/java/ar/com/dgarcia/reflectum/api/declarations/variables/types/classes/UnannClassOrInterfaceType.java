package ar.com.dgarcia.reflectum.api.declarations.variables.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannReferenceType;

/**
 * This type represents a class or interface variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannClassOrInterfaceType extends UnannReferenceType {
}
