package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPipe;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the bitwise OR operator symbol "|"
 * Created by kfgodel on 05/08/14.
 */
public interface BitwiseOrOperatorSymbol extends OperatorSymbol {
    AsciiPipe character();
}
