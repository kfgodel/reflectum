package ar.com.dgarcia.reflectum.api.declarations.references.arguments.bounds;

/**
 * This type represents a lower bound for a type wildcard
 * Created by kfgodel on 09/08/14.
 */
public interface LowerWildcardBoundDeclaration extends WildcardBoundsDeclaration {

    /**
     * @return The "super" word to indicate this is a lower bound
     */
    @Override
    String discriminator();
}
