package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal;

/**
 * This type represents a character present in a string literal
 * Created by kfgodel on 05/08/14.
 */
public interface StringCharacter {
}
