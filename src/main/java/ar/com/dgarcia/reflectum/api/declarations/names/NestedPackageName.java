package ar.com.dgarcia.reflectum.api.declarations.names;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents a package name that is nested in other packages
 * Created by kfgodel on 09/08/14.
 */
public interface NestedPackageName extends PackageName, NestedName {

    /**
     * @return The name of the immediate parent
     */
    PackageName parentPackage();
}
