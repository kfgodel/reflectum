package ar.com.dgarcia.reflectum.api.declarations.types.classes.enums;

import java.util.stream.Stream;

/**
 * This type represents a list of enum constants declarations
 * Created by kfgodel on 14/08/14.
 */
public interface EnumConstantList {

    /**
     * @return The first constant of the list
     */
    EnumConstant firstConstant();

    /**
     * @return The rest of constants
     */
    Stream<NonFirstEnumConstant> restOfConstant();
}
