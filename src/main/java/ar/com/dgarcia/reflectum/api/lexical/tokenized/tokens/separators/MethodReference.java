package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.separators;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiColon;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Separator;

/**
 * This type represents the method reference symbol
 * Created by kfgodel on 05/08/14.
 */
public interface MethodReference extends Separator {

    /**
     * @return The first of the sequence
     */
    AsciiColon firstColon();

    /**
     * @return Second and last in the sequence
     */
    AsciiColon secondColon();
}
