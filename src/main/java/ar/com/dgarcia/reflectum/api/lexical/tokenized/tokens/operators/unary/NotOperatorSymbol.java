package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.unary;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAdmiration;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the "!" operator symbol
 * Created by kfgodel on 05/08/14.
 */
public interface NotOperatorSymbol extends OperatorSymbol {

    /**
     * @return The character that represents this operator
     */
    AsciiAdmiration character();
}
