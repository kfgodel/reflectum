package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal;

/**
 * This type represents the integer suffix for long types
 * Created by kfgodel on 03/08/14.
 */
public interface IntegerTypeSuffix {
}
