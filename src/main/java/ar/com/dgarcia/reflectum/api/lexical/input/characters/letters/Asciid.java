package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal.FloatTypeSuffix;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;

/**
 * This type represents the ascii "d" character
 * Created by kfgodel on 04/08/14.
 */
public interface Asciid extends UnicodeInputCharacter, HexDigit, FloatTypeSuffix {

    /**
     * @return The "d" character
     */
    @Override
    Character character();
}
