package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits;

import java.util.stream.Stream;

/**
 * This type represents a sequence of hex digits and underscores
 * Created by kfgodel on 03/08/14.
 */
public interface HexDigitsAndUnderscores {

    /**
     * @return The first symbol of the sequence
     */
    HexDigitOrUnderscore firstSymbol();

    /**
     * @return The rest of symbols
     */
    Stream<HexDigitOrUnderscore> tailSymbols();
}
