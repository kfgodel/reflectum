package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.symbols;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiSlash;

/**
 * This type represents the symbol used to indicate the start of a line comment
 * Created by kfgodel on 03/08/14.
 */
public interface LineCommentSymbol {

    /**
     * @return First char of the two char sequence
     */
    AsciiSlash firstSlash();

    /**
     * @return Second char of the two char sequence
     */
    AsciiSlash secondSlash();
}
