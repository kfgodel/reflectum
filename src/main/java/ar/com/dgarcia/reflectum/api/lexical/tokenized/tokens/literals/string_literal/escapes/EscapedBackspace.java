package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciib;

/**
 * This type represents the escaped backspace
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedBackspace extends EscapedCharacter {

    /**
     * @return The character for backspace
     */
    Asciib backspaceMarker();
}
