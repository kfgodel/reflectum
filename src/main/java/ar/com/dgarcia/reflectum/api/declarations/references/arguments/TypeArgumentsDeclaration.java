package ar.com.dgarcia.reflectum.api.declarations.references.arguments;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiGt;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiLt;

/**
 * This type represents the declaration of type arguments
 * Created by kfgodel on 08/08/14.
 */
public interface TypeArgumentsDeclaration {
    /**
     * @return Argument list opening symbol
     */
    AsciiLt opening();

    /**
     * @return The list of type arguments
     */
    TypeArgumentListDeclaration list();

    /**
     * @return argument list closing symbol
     */
    AsciiGt closing();
}
