package ar.com.dgarcia.reflectum.api.declarations.references.references.arrays;

import ar.com.dgarcia.reflectum.api.declarations.references.custom.AnnotatedDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftBracket;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightBracket;

/**
 * This type represents the declaration of a single array dimension
 * Created by kfgodel on 08/08/14.
 */
public interface Dim extends AnnotatedDeclaration {

    /**
     * @return Start of empty brackets
     */
    AsciiLeftBracket opening();

    /**
     * @return End of empty brackets
     */
    AsciiRightBracket closing();
}
