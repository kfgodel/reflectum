package ar.com.dgarcia.reflectum.api.declarations.variables;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents the second and so variable declarators in a list
 * Created by kfgodel on 12/08/14.
 */
public interface NonFirstVariableDeclarator {

    /**
     * @return List separator
     */
    AsciiComma separator();

    /**
     * @return The declarator
     */
    VariableDeclarator declarator();
}
