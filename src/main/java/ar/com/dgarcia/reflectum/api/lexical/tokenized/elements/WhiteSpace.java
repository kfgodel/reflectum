package ar.com.dgarcia.reflectum.api.lexical.tokenized.elements;

/**
 * This type represents a tokenized white space
 * Created by kfgodel on 03/08/14.
 */
public interface WhiteSpace extends InputElement {
}
