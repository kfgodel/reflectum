package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.digits.Ascii0;

/**
 * This type represents an octal numeral literal
 * Created by kfgodel on 03/08/14.
 */
public interface OctalNumeral {

    /**
     * @return The "0" escape symbol
     */
    Ascii0 escapeDigit();
}
