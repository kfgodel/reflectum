package ar.com.dgarcia.reflectum.api.lexical.input.lines;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a character contained inside a language line construct
 * Created by kfgodel on 03/08/14.
 */
public interface InputCharacter extends UnicodeInputCharacter {

    /**
     * @return Any character except CR or LF
     */
    @Override
    Character character();
}
