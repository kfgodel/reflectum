package ar.com.dgarcia.reflectum.api.lexical.input.characters.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.NonZeroDigit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;

/**
 * This type represents the ascii "9" character
 * Created by kfgodel on 04/08/14.
 */
public interface Ascii9 extends UnicodeInputCharacter, Digit, HexDigit, NonZeroDigit {

    /**
     * @return The "9" character
     */
    @Override
    Character character();
}
