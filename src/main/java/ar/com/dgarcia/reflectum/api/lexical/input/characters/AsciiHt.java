package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.WhiteSpace;

/**
 * This type represents the Ascii horizontal tab character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiHt extends UnicodeInputCharacter, WhiteSpace {

    /**
     * @return The ascii "\t" character
     */
    @Override
    Character character();
}
