package ar.com.dgarcia.reflectum.api.declarations.variables.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.annotations.AnnotationDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.arguments.TypeArgumentsDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents a nested class variable type
 * Created by kfgodel on 12/08/14.
 */
public interface NestedUnannClassType extends UnannClassType{

    /**
     * @return Parent type
     */
    UnannClassOrInterfaceType outerType();

    /**
     * @return Nesting separator symbol
     */
    AsciiDot nestingSeparator();

    /**
     * @return Modifying annotations
     */
    Stream<AnnotationDeclaration> modifiers();

    /**
     * @return Class identifier
     */
    Identifier classIdentifier();

    /**
     * @return Class type arguments
     */
    Optional<TypeArgumentsDeclaration> typeArguments();
}
