package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.conditional;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPipe;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the conditional OR operator symbol "||"
 * Created by kfgodel on 05/08/14.
 */
public interface ConditionalOrOperatorSymbol extends OperatorSymbol {

    AsciiPipe firstCharacter();
    AsciiPipe secondCharacter();
}
