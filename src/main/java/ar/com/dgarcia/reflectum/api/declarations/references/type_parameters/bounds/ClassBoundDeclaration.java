package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.bounds;

import ar.com.dgarcia.reflectum.api.declarations.references.references.ClassOrInterfaceTypeReference;

import java.util.stream.Stream;

/**
 * This type represents the declaration of a type bound using a class or interface
 * Created by kfgodel on 09/08/14.
 */
public interface ClassBoundDeclaration extends TypeBoundDeclaration {

    /**
     * @return The mandatory first type
     */
    ClassOrInterfaceTypeReference firstType();

    /**
     * @return Additional bounds
     */
    Stream<AdditionalBoundDeclaration> extraBounds();
}
