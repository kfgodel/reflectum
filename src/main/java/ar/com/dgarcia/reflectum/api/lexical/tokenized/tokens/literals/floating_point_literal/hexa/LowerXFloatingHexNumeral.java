package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.digits.Ascii0;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciix;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigits;

import java.util.Optional;

/**
 * This type represents one of the hex significand forms
 * Created by kfgodel on 04/08/14.
 */
public interface LowerXFloatingHexNumeral extends HexSignificand {
    /**
     * @return The escape digit to start the numeral
     */
    Ascii0 escapeDigit();

    /**
     * @return The hex numeral marker
     */
    Asciix hexMarker();

    /**
     * @return The integer part
     */
    Optional<HexDigits> integerDigits();

    /**
     * @return The fractional separator
     */
    AsciiDot fractionalSeparator();

    /**
     * @return The fraction part digits
     */
    HexDigit fractionDigits();

}
