package ar.com.dgarcia.reflectum.api.declarations.references;

import ar.com.dgarcia.reflectum.api.declarations.references.custom.AnnotatedDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.references.primitives.PrimitiveTypeKeyword;

/**
 * This type represents one of the primitive types
 * Created by kfgodel on 06/08/14.
 */
public interface PrimitiveTypeReference extends TypeReference, AnnotatedDeclaration {

    /**
     * @return One of the primitive types keyword
     */
    PrimitiveTypeKeyword typeKeyword();
}
