package ar.com.dgarcia.reflectum.api.declarations.imports;

import ar.com.dgarcia.reflectum.api.declarations.names.TypeName;

/**
 * This type represents an import declaration for an imported type
 * Created by kfgodel on 03/08/14.
 */
public interface SingleTypeImportDeclaration extends ImportDeclaration {

    /**
     * @return The name of the type imported
     */
    TypeName importSource();
}
