package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits;

import java.util.Optional;

/**
 * This type represents an octal digit sequence with underscores
 * Created by kfgodel on 03/08/14.
 */
public interface UnderscoredOctalDigits extends OctalDigits{
    /**
     * @return Starting octal digit
     */
    OctalDigit firstDigit();

    /**
     * @return Sequence of octal and underscores
     */
    Optional<OctalDigitsAndUnderscores> middle();

    /**
     * @return Ending digit
     */
    OctalDigit lastDigit();
}
