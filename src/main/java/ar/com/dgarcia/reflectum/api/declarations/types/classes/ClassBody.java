package ar.com.dgarcia.reflectum.api.declarations.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.ClassBodyDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftCurly;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightCurly;

import java.util.stream.Stream;

/**
 * This type represents the body declaration of a class
 * Created by kfgodel on 11/08/14.
 */
public interface ClassBody {

    /**
     * @return Starting symbol
     */
    AsciiLeftCurly opening();

    /**
     * @return Definition of the class
     */
    Stream<ClassBodyDeclaration> declarations();

    /**
     * @return Ending symbol
     */
    AsciiRightCurly closing();
}
