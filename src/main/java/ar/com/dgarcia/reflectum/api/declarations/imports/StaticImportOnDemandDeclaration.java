package ar.com.dgarcia.reflectum.api.declarations.imports;

import ar.com.dgarcia.reflectum.api.declarations.names.TypeName;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;

/**
 * This type represents an on-demand static import (with wildcard)
 * Created by kfgodel on 11/08/14.
 */
public interface StaticImportOnDemandDeclaration extends StaticImportDeclaration {

    /**
     * @return The source type
     */
    TypeName importSource();

    /**
     * @return The symbol that imports everything
     */
    AsciiDot importFilter();
}
