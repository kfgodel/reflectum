package ar.com.dgarcia.reflectum.api.declarations.expressions;

import ar.com.dgarcia.reflectum.api.declarations.variables.VariableInitializer;

/**
 * This type represents a java expression
 * Created by kfgodel on 12/08/14.
 */
public interface Expression extends VariableInitializer {
}
