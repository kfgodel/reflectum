package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.IntegerLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.DecimalNumeral;

import java.util.Optional;

/**
 * This type represents a base 10 integer value
 * Created by kfgodel on 03/08/14.
 */
public interface DecimalIntegerLiteral extends IntegerLiteral {

    /**
     * @return The numeral value
     */
    DecimalNumeral numeral();

    /**
     * @return The type of value
     */
    Optional<IntegerTypeSuffix> suffix();
}
