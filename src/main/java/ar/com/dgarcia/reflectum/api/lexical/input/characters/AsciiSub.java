package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the Ascii SUB character also known as Control-Z
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiSub extends UnicodeInputCharacter {

    /**
     * @return The Ascii.SUB character
     */
    @Override
    Character character();
}
