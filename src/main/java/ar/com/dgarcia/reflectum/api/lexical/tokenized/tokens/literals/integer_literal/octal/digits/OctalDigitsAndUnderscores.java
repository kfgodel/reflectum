package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits;

import java.util.stream.Stream;

/**
 * This type represents a sequence of octal digit and underscores
 * Created by kfgodel on 03/08/14.
 */
public interface OctalDigitsAndUnderscores {

    /**
     * @return First digit or underscore
     */
    OctalDigitOrUnderscore firstSymbol();

    /**
     * @return the rest of symbols
     */
    Stream<OctalDigitOrUnderscore> tailSymbols();
}
