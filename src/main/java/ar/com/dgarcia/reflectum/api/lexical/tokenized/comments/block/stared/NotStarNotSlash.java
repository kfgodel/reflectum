package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block.stared;

/**
 * This type represents a character that is not a "*" or a "/"
 * Created by kfgodel on 03/08/14.
 */
public interface NotStarNotSlash {
}
