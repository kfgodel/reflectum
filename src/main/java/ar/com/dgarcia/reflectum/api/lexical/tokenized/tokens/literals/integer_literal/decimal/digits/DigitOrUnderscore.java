package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits;

/**
 * This type represents a digit or underscore symbol
 * Created by kfgodel on 03/08/14.
 */
public interface DigitOrUnderscore {
}
