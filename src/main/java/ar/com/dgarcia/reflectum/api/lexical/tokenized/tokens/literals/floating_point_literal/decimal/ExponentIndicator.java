package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal;

/**
 * This type represents the symbol used as an exponent mark in a floating point number
 * Created by kfgodel on 04/08/14.
 */
public interface ExponentIndicator {
}
