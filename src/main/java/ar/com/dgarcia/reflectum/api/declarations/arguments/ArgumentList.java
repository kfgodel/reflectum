package ar.com.dgarcia.reflectum.api.declarations.arguments;

import ar.com.dgarcia.reflectum.api.declarations.expressions.Expression;
import ar.com.dgarcia.reflectum.api.declarations.expressions.NonFirstExpression;

import java.util.stream.Stream;

/**
 * This type represents a list of arguments
 * Created by kfgodel on 12/08/14.
 */
public interface ArgumentList {

    /**
     * @return The first expression of the list
     */
    Expression firstExpression();

    /**
     * @return The rest of expressions
     */
    Stream<NonFirstExpression> rest();
}
