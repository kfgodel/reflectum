package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.WhiteSpace;

/**
 * This type represents the ascii Space character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiSp extends UnicodeInputCharacter, WhiteSpace {

    /**
     * @return The ascii " " character
     */
    @Override
    Character character();
}
