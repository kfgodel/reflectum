package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.FloatingPointLiteral;

/**
 * This type represents a decimal floating point value
 * Created by kfgodel on 04/08/14.
 */
public interface DecimalFloatingPointLiteral extends FloatingPointLiteral {
}
