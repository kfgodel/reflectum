package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii "X" character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiX extends UnicodeInputCharacter {

    /**
     * @return The "X" character
     */
    @Override
    Character character();
}
