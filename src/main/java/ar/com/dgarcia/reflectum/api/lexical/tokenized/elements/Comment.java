package ar.com.dgarcia.reflectum.api.lexical.tokenized.elements;

/**
 * This type represents a tokenized comment
 * Created by kfgodel on 03/08/14.
 */
public interface Comment extends InputElement {
}
