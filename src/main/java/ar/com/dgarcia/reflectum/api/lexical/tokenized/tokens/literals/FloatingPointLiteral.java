package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Literal;

/**
 * This type represents a floating point literal value as "1.2"
 * Created by kfgodel on 03/08/14.
 */
public interface FloatingPointLiteral extends Literal {
}
