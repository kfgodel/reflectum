package ar.com.dgarcia.reflectum.api.declarations.imports;

import ar.com.dgarcia.reflectum.api.declarations.names.TypeName;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

/**
 * This type represents a single static import
 * Created by kfgodel on 11/08/14.
 */
public interface SingleStaticImportDeclaration extends StaticImportDeclaration {

    /**
     * @return The source type
     */
    TypeName importSource();

    /**
     * @return The expression to define imported elements
     */
    Identifier importFilter();
}
