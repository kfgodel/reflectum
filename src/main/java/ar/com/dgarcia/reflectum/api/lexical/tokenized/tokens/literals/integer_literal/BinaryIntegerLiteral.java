package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.IntegerLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.BinaryNumeral;

import java.util.Optional;

/**
 * This type represents a base 2 integer value
 * Created by kfgodel on 03/08/14.
 */
public interface BinaryIntegerLiteral extends IntegerLiteral{

    /**
     * @return the numeral value
     */
    BinaryNumeral numeral();

    /**
     * @return The type of value
     */
    Optional<IntegerTypeSuffix> suffix();

}
