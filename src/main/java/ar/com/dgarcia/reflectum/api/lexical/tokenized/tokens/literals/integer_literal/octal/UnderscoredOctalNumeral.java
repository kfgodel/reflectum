package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.Underscores;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.digits.OctalDigits;

/**
 * This type represents an octal numeral with underscores
 * Created by kfgodel on 03/08/14.
 */
public interface UnderscoredOctalNumeral extends OctalNumeral {

    /**
     * @return Separators underscores
     */
    Underscores underscores();

    /**
     * @return Octal value digits
     */
    OctalDigits digits();
}
