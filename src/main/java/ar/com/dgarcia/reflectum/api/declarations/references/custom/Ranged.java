package ar.com.dgarcia.reflectum.api.declarations.references.custom;

/**
 * This type represents a ranged value entity
 * Created by kfgodel on 07/08/14.
 */
public interface Ranged {

    /**
     * @return The range of valid values
     */
    ValueRange valueRanges();
}
