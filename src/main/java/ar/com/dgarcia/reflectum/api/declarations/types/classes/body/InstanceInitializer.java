package ar.com.dgarcia.reflectum.api.declarations.types.classes.body;

/**
 * This type represents the class instance initialization declaration
 * Created by kfgodel on 11/08/14.
 */
public interface InstanceInitializer extends ClassBodyDeclaration{
}
