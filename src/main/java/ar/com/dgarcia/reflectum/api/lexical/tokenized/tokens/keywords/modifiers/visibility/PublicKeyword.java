package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers.visibility;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.ClassModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors.ConstructorModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.fields.FieldModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodModifier;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "public" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface PublicKeyword extends Keyword, ClassModifier, FieldModifier, MethodModifier, ConstructorModifier {
}
