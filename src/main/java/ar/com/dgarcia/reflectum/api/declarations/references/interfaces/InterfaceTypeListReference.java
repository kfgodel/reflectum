package ar.com.dgarcia.reflectum.api.declarations.references.interfaces;

import ar.com.dgarcia.reflectum.api.declarations.references.references.classes.InterfaceTypeReference;

import java.util.stream.Stream;

/**
 * This type represents a list of interfaces
 * Created by kfgodel on 11/08/14.
 */
public interface InterfaceTypeListReference {

    /**
     * @return The first referenced interface
     */
    InterfaceTypeReference firstInterface();

    /**
     * @return Rest of the interfaces
     */
    Stream<InterfaceTypeReference> extra();
}
