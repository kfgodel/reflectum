package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii asterisk symbol
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiAsterisk extends UnicodeInputCharacter {

    /**
     * @return The "*" character
     */
    @Override
    Character character();
}
