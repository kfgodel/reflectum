package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block;

/**
 * This type represents a comment not started with an asterisk
 * Created by kfgodel on 03/08/14.
 */
public interface NotStarStartedTail extends CommentTail {

    /**
     * @return The starting character that is not an "*"
     */
    NotStar startCharacter();

    /**
     * @return Rest of the comment
     */
    CommentTail tail();
}
