package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal.ExponentIndicator;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.hex.digits.HexDigit;

/**
 * This type represents the ascii "E" character
 * Created by kfgodel on 04/08/14.
 */
public interface AsciiE extends UnicodeInputCharacter, HexDigit, ExponentIndicator {

    /**
     * @return The "E" character
     */
    @Override
    Character character();
}
