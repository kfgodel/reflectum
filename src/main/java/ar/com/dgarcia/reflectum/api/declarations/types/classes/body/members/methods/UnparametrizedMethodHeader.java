package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods;

import java.util.Optional;

/**
 * This type represents a simple method header
 * Created by kfgodel on 12/08/14.
 */
public interface UnparametrizedMethodHeader extends MethodHeader {

    /**
     * @return Method result declaration
     */
    ResultDeclaration result();

    /**
     * @return Method signatore declaration
     */
    MethodDeclarator declarator();

    /**
     * @return Exceptions declaration
     */
    Optional<ThrowsDeclaration> throwsClause();
}
