package ar.com.dgarcia.reflectum.api.declarations.types.classes.body;

/**
 * This type represents the contents of a class body
 * Created by kfgodel on 11/08/14.
 */
public interface ClassBodyDeclaration {
}
