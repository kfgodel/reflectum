package ar.com.dgarcia.reflectum.api.declarations.types.classes.body;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors.ConstructorDeclarator;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors.ConstructorModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.ThrowsDeclaration;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents the constructor declaration of class
 * Created by kfgodel on 11/08/14.
 */
public interface ConstructorDeclaration extends ClassBodyDeclaration {

    /**
     * @return The optional modifiers
     */
    Stream<ConstructorModifier> modifiers();

    /**
     * @return The constructore header
     */
    ConstructorDeclarator declarator();

    /**
     * @return The exceptions clause declaration
     */
    Optional<ThrowsDeclaration> throwsDeclaration();

    /**
     * @return The defining body
     */
    ConstructorBody methodBody();
}
