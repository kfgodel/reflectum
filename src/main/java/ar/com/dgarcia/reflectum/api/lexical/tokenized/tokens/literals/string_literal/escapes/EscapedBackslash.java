package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiBackslash;

/**
 * This type represents the escaped backslash character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedBackslash extends EscapedCharacter {

    /**
     * @return The character used to indicate a backslash
     */
    AsciiBackslash backslashMarker();
}
