package ar.com.dgarcia.reflectum.api.declarations.types;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.ClassMemberDeclaration;

/**
 * This type represents an interface declaration
 * Created by kfgodel on 11/08/14.
 */
public interface InterfaceDeclaration  extends TypeDeclaration, ClassMemberDeclaration{
}
