package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal;

/**
 * This type represents one of the sign characters
 * Created by kfgodel on 04/08/14.
 */
public interface Sign {
}
