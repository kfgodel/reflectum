package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.FloatingPointLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.decimal.FloatTypeSuffix;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa.BinaryExponent;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.hexa.HexSignificand;

import java.util.Optional;

/**
 * This type represents a floating point hexadecimal value
 * Created by kfgodel on 04/08/14.
 */
public interface HexadecimalFloatingPointLiteral extends FloatingPointLiteral {

    /**
     * @return The numeral part
     */
    HexSignificand significand();

    /**
     * @return The exponent part
     */
    BinaryExponent exponent();

    /**
     * @return The floating point type suffix
     */
    Optional<FloatTypeSuffix> typeSuffix();
}
