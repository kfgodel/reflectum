package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.digits.Ascii0;

/**
 * This type represents a binary numeral literal
 * Created by kfgodel on 04/08/14.
 */
public interface BinaryNumeral {

    /**
     * @return The starting escape character
     */
    Ascii0 escapeDigit();
}
