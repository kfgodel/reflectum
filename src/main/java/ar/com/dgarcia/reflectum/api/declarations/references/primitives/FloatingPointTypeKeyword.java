package ar.com.dgarcia.reflectum.api.declarations.references.primitives;

/**
 * This type represents one of the floating point types
 * Created by kfgodel on 07/08/14.
 */
public interface FloatingPointTypeKeyword extends NumericTypeKeyword {
}
