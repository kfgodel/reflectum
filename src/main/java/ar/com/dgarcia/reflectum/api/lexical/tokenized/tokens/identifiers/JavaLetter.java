package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.identifiers;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a java letter character
 * Created by kfgodel on 03/08/14.
 */
public interface JavaLetter extends UnicodeInputCharacter {

    /**
     * @return Any character that is a Java letter
     */
    @Override
    Character character();
}
