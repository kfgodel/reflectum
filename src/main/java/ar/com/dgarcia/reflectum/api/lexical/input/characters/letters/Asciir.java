package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii "r" character
 * Created by kfgodel on 05/08/14.
 */
public interface Asciir extends UnicodeInputCharacter {

    /**
     * @return The "r" character
     */
    @Override
    Character character();
}
