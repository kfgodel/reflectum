package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.WhiteSpace;

/**
 * This type represents the ascii Form Feed character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiFf extends UnicodeInputCharacter, WhiteSpace {

    /**
     * @return The ascii "\f"
     */
    @Override
    Character character();
}
