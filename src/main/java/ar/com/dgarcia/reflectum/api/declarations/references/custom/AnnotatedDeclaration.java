package ar.com.dgarcia.reflectum.api.declarations.references.custom;

import ar.com.dgarcia.reflectum.api.declarations.references.annotations.AnnotationDeclaration;

import java.util.stream.Stream;

/**
 * This type is an artificial type for entities that may have annotations
 * Created by kfgodel on 07/08/14.
 */
public interface AnnotatedDeclaration {

    /**
     * @return The optional type annotations
     */
    Stream<AnnotationDeclaration> annotation();

}
