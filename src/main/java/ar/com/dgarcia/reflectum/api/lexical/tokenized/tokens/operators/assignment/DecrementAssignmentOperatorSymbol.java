package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiMinus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the decrement and assign operator symbol "-="
 * Created by kfgodel on 05/08/14.
 */
public interface DecrementAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiMinus firstCharacter();
    AsciiEqual secondCharacter();
}