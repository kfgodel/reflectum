package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods;

/**
 * This type represents a method result declaration
 * Created by kfgodel on 12/08/14.
 */
public interface ResultDeclaration {
}
