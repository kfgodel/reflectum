package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "default" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface DefaultKeyword extends Keyword {
}
