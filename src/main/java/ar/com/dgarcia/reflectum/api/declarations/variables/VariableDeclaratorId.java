package ar.com.dgarcia.reflectum.api.declarations.variables;

import ar.com.dgarcia.reflectum.api.declarations.references.references.arrays.Dims;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;

/**
 * This type represents a variable identifier
 * Created by kfgodel on 12/08/14.
 */
public interface VariableDeclaratorId {

    /**
     * @return Variable name
     */
    Identifier identifier();

    /**
     * @return Optional dimensions for array
     */
    Optional<Dims> dimensions();
}
