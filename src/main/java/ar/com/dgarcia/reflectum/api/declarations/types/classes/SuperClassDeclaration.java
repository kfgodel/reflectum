package ar.com.dgarcia.reflectum.api.declarations.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.references.classes.ClassTypeReference;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.relations.ExtendsKeyword;

/**
 * This type represents the superclass relation declaration
 * Created by kfgodel on 11/08/14.
 */
public interface SuperClassDeclaration {

    /**
     * @return The keyword to declare the relation
     */
    ExtendsKeyword extendsKeyword();

    /**
     * @return The class reference
     */
    ClassTypeReference superClass();
}
