package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods;

/**
 * This type represents the body of a method
 * Created by kfgodel on 12/08/14.
 */
public interface MethodBody {
}
