package ar.com.dgarcia.reflectum.api.declarations.references.references;

import ar.com.dgarcia.reflectum.api.declarations.references.ReferenceTypeReference;

/**
 * This type represents the declaration of class or interface type
 * Created by kfgodel on 08/08/14.
 */
public interface ClassOrInterfaceTypeReference extends ReferenceTypeReference {
}
