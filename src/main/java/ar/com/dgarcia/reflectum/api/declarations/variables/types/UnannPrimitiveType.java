package ar.com.dgarcia.reflectum.api.declarations.variables.types;

import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannType;

/**
 * This type represents a primitive variable type
 * Created by kfgodel on 12/08/14.
 */
public interface UnannPrimitiveType  extends UnannType {
}
