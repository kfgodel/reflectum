package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.letters.Asciir;

/**
 * This type represents the escaped carriage return character
 * Created by kfgodel on 05/08/14.
 */
public interface EscapedCarriageReturn extends EscapedCharacter {

    /**
     * @return The character used to represent the cr
     */
    Asciir carriageReturnMarker();
}
