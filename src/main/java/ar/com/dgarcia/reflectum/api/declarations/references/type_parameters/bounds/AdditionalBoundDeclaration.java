package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.bounds;

import ar.com.dgarcia.reflectum.api.declarations.references.references.classes.InterfaceTypeReference;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAmpersand;

/**
 * This type represents the declaration of an additional type bound
 * Created by kfgodel on 09/08/14.
 */
public interface AdditionalBoundDeclaration {
    /**
     * @return Symbol used to separate declarations
     */
    AsciiAmpersand separator();

    /**
     * @return The interface type declaration acting as bound
     */
    InterfaceTypeReference boundingType();
}
