package ar.com.dgarcia.reflectum.api.declarations.references.primitives;

import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannPrimitiveType;

/**
 * This type represents one of the numeric primitive types
 * Created by kfgodel on 07/08/14.
 */
public interface NumericTypeKeyword extends PrimitiveTypeKeyword, UnannPrimitiveType {

    /**
     * @return The number of bits used to represent this type values
     */
    int bitSize();
}
