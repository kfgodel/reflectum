package ar.com.dgarcia.reflectum.api.lexical.input.characters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.floating_point_literal.Sign;

/**
 * This type represents the ascii "-" character
 * Created by kfgodel on 04/08/14.
 */
public interface AsciiMinus extends UnicodeInputCharacter, Sign {

    /**
     * @return The "-" character
     */
    @Override
    Character character();
}
