package ar.com.dgarcia.reflectum.api.declarations.invocations.constructors;

import ar.com.dgarcia.reflectum.api.declarations.invocations.ExplicitConstructorInvocation;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.pseudo_variables.SuperKeyword;

/**
 * This type represents a super constructor call
 * Created by kfgodel on 13/08/14.
 */
public interface SuperConstructorInvocation extends ExplicitConstructorInvocation {
    /**
     * @return The kwyword to indicate a super constructor
     */
    SuperKeyword superKeyword();
}
