package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents the second or more type parameter declaration
 * Created by kfgodel on 11/08/14.
 */
public interface NonFirstTypeParameterDeclaration {

    /**
     * @return Type parameter separator symbol
     */
    AsciiComma separator();

    /**
     * @return The type parameter declaration
     */
    TypeParameterDeclaration typeParameter();
}
