package ar.com.dgarcia.reflectum.api.declarations.initializers;

import ar.com.dgarcia.reflectum.api.declarations.variables.VariableInitializer;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftCurly;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightCurly;

import java.util.Optional;

/**
 * This type represents an array variable initilizer
 * Created by kfgodel on 12/08/14.
 */
public interface ArrayInitializer extends VariableInitializer {

    /**
     * @return Initializer opening symbol
     */
    AsciiLeftCurly opening();

    /**
     * @return array value declarations
     */
    Optional<VariableInitializerList> initializerList();

    /**
     * @return List separator symbol
     */
    Optional<AsciiComma> separator();

    /**
     * @return initializer closing symbol
     */
    AsciiRightCurly closing();
}
