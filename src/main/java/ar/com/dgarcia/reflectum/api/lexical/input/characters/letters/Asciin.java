package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii "n" character
 * Created by kfgodel on 05/08/14.
 */
public interface Asciin extends UnicodeInputCharacter {

    /**
     * @return The "n" character
     */
    @Override
    Character character();
}
