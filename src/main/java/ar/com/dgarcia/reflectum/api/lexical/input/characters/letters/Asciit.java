package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the Ascii "t" character
 * Created by kfgodel on 05/08/14.
 */
public interface Asciit extends UnicodeInputCharacter {

    /**
     * @return The "t" character
     */
    @Override
    Character character();
}
