package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.identifiers;

import java.util.stream.Stream;

/**
 * This type represents the sequence of characters that form an identifier
 * Created by kfgodel on 03/08/14.
 */
public interface IdentifierChars {

    /**
     * @return The starting letter character
     */
    JavaLetter startLetter();

    /**
     * @return The rest of characters
     */
    Stream<JavaLetterOrDigit> tail();
}
