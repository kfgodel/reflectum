package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.bounds;

import ar.com.dgarcia.reflectum.api.declarations.references.references.TypeVariableReference;

/**
 * This type represents the declaration of a type variable bounded type parameter bound declaration (basically a parameter bounded to other parameter)
 * Created by kfgodel on 09/08/14.
 */
public interface VariableBoundDeclaration extends TypeBoundDeclaration {

    /**
     * @return Variable reference that binds
     */
    TypeVariableReference variable();
}
