package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.Digits;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits.NonZeroDigit;

import java.util.Optional;

/**
 * This type represents a decimal numeral without underscores
 * Created by kfgodel on 03/08/14.
 */
public interface NonUnderscoredDecimalNumeral extends DecimalNumeral {

    /**
     * @return Starting digit
     */
    NonZeroDigit firstDigit();

    /**
     * @return Rest of digits
     */
    Optional<Digits> tailDigits();
}
