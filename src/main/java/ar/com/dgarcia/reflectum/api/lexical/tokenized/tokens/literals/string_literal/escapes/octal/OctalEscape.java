package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes.octal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes.EscapedCharacter;

/**
 * This type represents an octal escaped sequence
 * Created by kfgodel on 05/08/14.
 */
public interface OctalEscape extends EscapedCharacter {
}
