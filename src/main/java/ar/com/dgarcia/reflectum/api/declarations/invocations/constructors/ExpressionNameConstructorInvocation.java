package ar.com.dgarcia.reflectum.api.declarations.invocations.constructors;

import ar.com.dgarcia.reflectum.api.declarations.invocations.ExplicitConstructorInvocation;
import ar.com.dgarcia.reflectum.api.declarations.names.ExpressionName;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.pseudo_variables.SuperKeyword;

/**
 * This type represents a super constructor call using an expression name
 * Created by kfgodel on 13/08/14.
 */
public interface ExpressionNameConstructorInvocation extends ExplicitConstructorInvocation {

    /**
     * @return The expression name
     */
    ExpressionName expresionName();

    /**
     * @return The nesting separator
     */
    AsciiDot nestingSeparator();

    /**
     * @return The super keyword to indicate a super contructor
     */
    SuperKeyword superKeyword();
}
