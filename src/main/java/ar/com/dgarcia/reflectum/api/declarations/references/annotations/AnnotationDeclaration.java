package ar.com.dgarcia.reflectum.api.declarations.references.annotations;

import ar.com.dgarcia.reflectum.api.declarations.packages.PackageModifier;
import ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.TypeParameterModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.ClassModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors.ConstructorModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.fields.FieldModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodModifier;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.enums.EnumConstantModifier;
import ar.com.dgarcia.reflectum.api.declarations.variables.VariableModifier;

/**
 * This type represents one of the possible annotations
 * Created by kfgodel on 07/08/14.
 */
public interface AnnotationDeclaration extends TypeParameterModifier, PackageModifier, ClassModifier, FieldModifier, MethodModifier, VariableModifier, ConstructorModifier, EnumConstantModifier {
}
