package ar.com.dgarcia.reflectum.api.declarations.types.classes.body;

import ar.com.dgarcia.reflectum.api.declarations.expressions.BlockDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers.StaticKeyword;

/**
 * This type represents the static class initializer declaration
 * Created by kfgodel on 11/08/14.
 */
public interface StaticInitializer extends ClassBodyDeclaration {
    /**
     * @return The static block starter keyword
     */
    StaticKeyword staticKeyword();

    /**
     * @return The block to be run statically
     */
    BlockDeclaration staticBlock();
}
