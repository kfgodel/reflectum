package ar.com.dgarcia.reflectum.api.declarations.types.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.TypeParametersDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.types.ClassDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.types.ClassKeyword;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents the declaration of a class
 * Created by kfgodel on 11/08/14.
 */
public interface NormalClassDeclaration extends ClassDeclaration {

    /**
     * @return The modifiers of the class declaration
     */
    Stream<ClassModifier> modifiers();

    /**
     * @return The keyword to start a class declaration
     */
    ClassKeyword classKeyword();

    /**
     * @return Identifier of the class
     */
    Identifier identifier();

    /**
     * @return Type parameters
     */
    Optional<TypeParametersDeclaration> typeParameters();

    /**
     * @return Super class reference
     */
    Optional<SuperClassDeclaration> superClass();

    /**
     * @return Super interfaces references
     */
    Stream<SuperInterfacesDeclaration> superInterfaces();

    /**
     * @return Class definition
     */
    ClassBody body();
}
