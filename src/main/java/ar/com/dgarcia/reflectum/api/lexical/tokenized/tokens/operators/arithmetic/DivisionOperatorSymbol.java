package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.arithmetic;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiSlash;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the division operator symbol "/"
 * Created by kfgodel on 05/08/14.
 */
public interface DivisionOperatorSymbol extends OperatorSymbol {
    AsciiSlash character();
}
