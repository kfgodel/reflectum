package ar.com.dgarcia.reflectum.api.declarations.parameters;

import ar.com.dgarcia.reflectum.api.declarations.references.annotations.AnnotationDeclaration;
import ar.com.dgarcia.reflectum.api.declarations.variables.VariableDeclaratorId;
import ar.com.dgarcia.reflectum.api.declarations.variables.VariableModifier;
import ar.com.dgarcia.reflectum.api.declarations.variables.types.UnannType;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;

import java.util.stream.Stream;

/**
 * This type represents the declaration of a variable arg parameter
 * Created by kfgodel on 12/08/14.
 */
public interface VariableArgFormalParameter extends LastFormalParameter {

    /**
     * @return The parameters modifiers
     */
    Stream<VariableModifier> modifiers();

    /**
     * @return The type of the parameter
     */
    UnannType paramterType();

    /**
     * @return Optional annotations
     */
    Stream<AnnotationDeclaration> annotations();

    /**
     * @return Three points symbol
     */
    AsciiDot firstDot();
    AsciiDot secondDot();
    AsciiDot thirdDot();

    /**
     * @return The parameter declarator id
     */
    VariableDeclaratorId declaratorId();
}
