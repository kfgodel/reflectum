package ar.com.dgarcia.reflectum.api.declarations.expressions;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.InstanceInitializer;
import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods.MethodBody;

/**
 * This type represents a declared block of code
 * Created by kfgodel on 12/08/14.
 */
public interface BlockDeclaration extends MethodBody, InstanceInitializer {
}
