package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers;

import ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.fields.FieldModifier;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "volatile" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface VolatileKeyword extends Keyword, FieldModifier {
}
