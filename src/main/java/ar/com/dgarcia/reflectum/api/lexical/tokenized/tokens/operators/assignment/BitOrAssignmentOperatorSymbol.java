package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPipe;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the bitwise OR and assign operator symbol "|="
 * Created by kfgodel on 05/08/14.
 */
public interface BitOrAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiPipe firstCharacter();
    AsciiEqual secondCharacter();
}
