package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiPercentage;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the division remainder and assign operator symbol "%="
 * Created by kfgodel on 05/08/14.
 */
public interface RemainderAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiPercentage firstCharacter();
    AsciiEqual secondCharacter();
}
