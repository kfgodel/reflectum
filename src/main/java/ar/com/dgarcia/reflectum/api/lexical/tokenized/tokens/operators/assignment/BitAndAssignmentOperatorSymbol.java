package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiAmpersand;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the bitwise AND and assign operator symbol "&="
 * Created by kfgodel on 05/08/14.
 */
public interface BitAndAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiAmpersand firstCharacter();
    AsciiEqual secondCharacter();
}
