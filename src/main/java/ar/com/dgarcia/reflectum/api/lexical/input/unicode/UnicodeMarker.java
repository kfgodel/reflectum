package ar.com.dgarcia.reflectum.api.lexical.input.unicode;

/**
 * This type represents the unicode scaped marker character "u"
 * Created by kfgodel on 03/08/14.
 */
public interface UnicodeMarker extends UnicodeInputCharacter {

    /**
     * @return The "u" character that identifies this marker
     */
    Character character();
}
