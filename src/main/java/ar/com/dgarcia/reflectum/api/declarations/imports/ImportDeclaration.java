package ar.com.dgarcia.reflectum.api.declarations.imports;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiSemicolon;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.declarations.ImportKeyword;

/**
 * This type represents a single import declaration
 * Created by kfgodel on 11/08/14.
 */
public interface ImportDeclaration {

    /**
     * @return the keyword to start an import declaration
     */
    ImportKeyword importKeyword();

    /**
     * @return Symbol to end an import declaration
     */
    AsciiSemicolon endSymbol();
}
