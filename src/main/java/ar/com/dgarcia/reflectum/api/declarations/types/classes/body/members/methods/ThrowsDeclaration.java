package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.members.methods;

import ar.com.dgarcia.reflectum.api.declarations.references.exceptions.ExceptionTypeListReference;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers.ThrowsKeyword;

/**
 * This type represents the declaration of a method throws list
 * Created by kfgodel on 12/08/14.
 */
public interface ThrowsDeclaration {

    /**
     * @return Keyword used to indicate the start of the list
     */
    ThrowsKeyword throwsKeyword();

    /**
     * @return The list of exception
     */
    ExceptionTypeListReference exceptionList();
}
