package ar.com.dgarcia.reflectum.api.declarations.references.references.arrays;

import java.util.stream.Stream;

/**
 * This type represents the declaration of an array dimensions
 * Created by kfgodel on 08/08/14.
 */
public interface Dims {

    /**
     * @return Mandatory dimension
     */
    Dim firstDimension();

    /**
     * @return Optional dimensions
     */
    Stream<Dim> extraDimensions();
}
