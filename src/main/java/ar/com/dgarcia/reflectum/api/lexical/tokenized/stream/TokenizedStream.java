package ar.com.dgarcia.reflectum.api.lexical.tokenized.stream;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiSub;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.elements.InputElement;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This type represents a reduced sequence of input elements from a {@link ar.com.dgarcia.reflectum.api.lexical.input.stream.LexicalStream}
 * Created by kfgodel on 03/08/14.
 */
public interface TokenizedStream {

    /**
     * @return The sequences of zero or more input elements that were tokenized
     */
    Stream<InputElement> elements();

    /**
     * @return The optional sub character at the end
     */
    Optional<AsciiSub>  sub();

}
