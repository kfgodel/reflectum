package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiLt;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the signed left shift and assign operator symbol "<<="
 * Created by kfgodel on 05/08/14.
 */
public interface SignedLeftShiftAssignmentOperatorSymbol extends OperatorSymbol {
    AsciiLt firstCharacter();
    AsciiLt secondCharacter();
    AsciiEqual thirdCharacter();
}