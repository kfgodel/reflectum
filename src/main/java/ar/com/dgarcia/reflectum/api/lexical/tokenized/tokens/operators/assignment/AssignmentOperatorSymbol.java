package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.assignment;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the "=" operator symbol
 * Created by kfgodel on 05/08/14.
 */
public interface AssignmentOperatorSymbol extends OperatorSymbol {

    /**
     * @return The character that represents this symbol
     */
    AsciiEqual character();
}
