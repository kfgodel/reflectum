package ar.com.dgarcia.reflectum.api.declarations.variables;

import java.util.Optional;

/**
 * This type represents a variable name declaration
 * Created by kfgodel on 12/08/14.
 */
public interface VariableDeclarator {

    /**
     * @return The variable name identifier
     */
    VariableDeclaratorId identifier();

    /**
     * @return The optional initialization
     */
    Optional<VariableInitializator> initializator();
}
