package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.escapes.octal;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a range of digits
 * Created by kfgodel on 05/08/14.
 */
public interface ZeroToThree extends UnicodeInputCharacter {

    /**
     * @return Any of 0 1 2 3
     */
    @Override
    Character character();
}
