package ar.com.dgarcia.reflectum.api.declarations.references.references.classes;

import ar.com.dgarcia.reflectum.api.declarations.references.arguments.TypeArgumentsDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Identifier;

import java.util.Optional;

/**
 * This type represents the declaration of a typical class type
 * Created by kfgodel on 08/08/14.
 */
public interface OuterClassTypeReference extends ClassTypeReference {
    /**
     * @return The name that identifies this class
     */
    Identifier identifier();

    /**
     * @return The arguments needed for this type
     */
    Optional<TypeArgumentsDeclaration> typeArguments();

}
