package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents the ascii "x" character
 * Created by kfgodel on 03/08/14.
 */
public interface Asciix extends UnicodeInputCharacter {

    /**
     * @return The "x" character
     */
    @Override
    Character character();
}
