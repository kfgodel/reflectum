package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.bit;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiNuflo;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the uniry bitwise complement symbol "~"
 * Created by kfgodel on 05/08/14.
 */
public interface BitwiseComplementOperatorSymbol extends OperatorSymbol {

    /**
     * @return The character that represents this operator
     */
    AsciiNuflo character();
}
