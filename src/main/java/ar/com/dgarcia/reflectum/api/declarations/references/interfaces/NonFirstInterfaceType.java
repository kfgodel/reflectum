package ar.com.dgarcia.reflectum.api.declarations.references.interfaces;

import ar.com.dgarcia.reflectum.api.declarations.references.references.classes.InterfaceTypeReference;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents the second and so interface in a list
 * Created by kfgodel on 11/08/14.
 */
public interface NonFirstInterfaceType {

    /**
     * @return List separator symbol
     */
    AsciiComma separator();

    /**
     * @return The referenced interface
     */
    InterfaceTypeReference interfaceReference();
}
