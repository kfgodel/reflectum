package ar.com.dgarcia.reflectum.api.declarations.types.classes.body;

/**
 * This type represents the declaration of a class member
 * Created by kfgodel on 11/08/14.
 */
public interface ClassMemberDeclaration extends ClassBodyDeclaration{
}
