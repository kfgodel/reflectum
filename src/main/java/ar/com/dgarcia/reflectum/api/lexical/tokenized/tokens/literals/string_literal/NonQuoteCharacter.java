package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal;

import ar.com.dgarcia.reflectum.api.lexical.input.lines.InputCharacter;

/**
 * This type is one of the possible string character
 * Created by kfgodel on 05/08/14.
 */
public interface NonQuoteCharacter extends StringCharacter, InputCharacter {

    /**
     * @return Any character except "\"" or "\\"
     */
    @Override
    Character character();
}
