package ar.com.dgarcia.reflectum.api.declarations.references.primitives;

/**
 * This type is an artificial supertype for keywords that represents primitive types
 * Created by kfgodel on 08/08/14.
 */
public interface PrimitiveTypeKeyword {
}
