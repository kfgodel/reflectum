package ar.com.dgarcia.reflectum.api.declarations.types.classes.enums;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiComma;

/**
 * This type represents the second an so enum constants declared on a list
 * Created by kfgodel on 14/08/14.
 */
public interface NonFirstEnumConstant {
    /**
     * @return The list separator
     */
    AsciiComma separator();

    /**
     * @return The constant
     */
    EnumConstant enumConstant();
}
