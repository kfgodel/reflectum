package ar.com.dgarcia.reflectum.api.declarations.variables;

import java.util.stream.Stream;

/**
 * This type represents a multi variable declaration
 * Created by kfgodel on 12/08/14.
 */
public interface VariableDeclaratorList {

    /**
     * @return The first variable declarator
     */
    VariableDeclarator firstDeclarator();

    /**
     * @return The rest of the declarators
     */
    Stream<NonFirstVariableDeclarator> extra();
}
