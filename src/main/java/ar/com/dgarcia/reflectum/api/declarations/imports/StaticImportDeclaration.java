package ar.com.dgarcia.reflectum.api.declarations.imports;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.modifiers.StaticKeyword;

/**
 * This type is an artificial superclass for common definition of static imports
 * Created by kfgodel on 11/08/14.
 */
public interface StaticImportDeclaration extends  ImportDeclaration {

    /**
     * @return The keyword that modifies this import as static
     */
    StaticKeyword staticKeyword();

    /**
     * @return separator symbol from source and imported element
     */
    AsciiDot separator();
}
