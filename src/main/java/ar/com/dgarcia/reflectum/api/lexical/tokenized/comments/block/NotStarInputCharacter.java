package ar.com.dgarcia.reflectum.api.lexical.tokenized.comments.block;

import ar.com.dgarcia.reflectum.api.lexical.input.lines.InputCharacter;

/**
 * This type represents a character taht is not a "*"
 * Created by kfgodel on 03/08/14.
 */
public interface NotStarInputCharacter extends InputCharacter, NotStar {

    /**
     * @return Any except "*", CR or LF
     */
    @Override
    Character character();
}
