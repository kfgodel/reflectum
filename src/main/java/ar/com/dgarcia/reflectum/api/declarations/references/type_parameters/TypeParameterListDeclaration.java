package ar.com.dgarcia.reflectum.api.declarations.references.type_parameters;

import java.util.stream.Stream;

/**
 * This type represents a list of type parameters
 * Created by kfgodel on 11/08/14.
 */
public interface TypeParameterListDeclaration {


    /**
     * @return The first type parameter
     */
    TypeParameterDeclaration firstParameter();

    /**
     * @return Resto of the type parameters
     */
    Stream<NonFirstTypeParameterDeclaration> extraParameters();
}
