package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.character_literal;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiQuote;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.CharacterLiteral;

/**
 * This type represents a character literal
 * Created by kfgodel on 05/08/14.
 */
public interface QuotedCharacter extends CharacterLiteral {

    /**
     * @return The initial character quote
     */
    AsciiQuote opening();

    /**
     * @return The character symbol
     */
    SingleCharacter character();

    /**
     * @return The end character quote
     */
    AsciiQuote closing();
}
