package ar.com.dgarcia.reflectum.api.declarations.types.classes.body.constructors;

import ar.com.dgarcia.reflectum.api.declarations.parameters.FormalParameterList;
import ar.com.dgarcia.reflectum.api.declarations.references.type_parameters.TypeParametersDeclaration;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiLeftParen;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiRightParen;

import java.util.Optional;

/**
 * This type represents the method header of a constructor
 * Created by kfgodel on 12/08/14.
 */
public interface ConstructorDeclarator {
    /**
     * @return The type parameters of this constructor
     */
    Optional<TypeParametersDeclaration> typeParameters();

    /**
     * @return The name of the enclosing type
     */
    SimpleTypeName enclosingType();

    /**
     * @return The initial symbol for the parameter list
     */
    AsciiLeftParen opening();

    /**
     * @return The list of constructor parameters
     */
    Optional<FormalParameterList> parameters();

    /**
     * @return the closing symbol for parameter list
     */
    AsciiRightParen closing();
}
