package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a binary digit
 * Created by kfgodel on 04/08/14.
 */
public interface BinaryDigit extends UnicodeInputCharacter, BinaryDigits, BinaryDigitOrUnderscore{

    /**
     * @return One of 0 or 1
     */
    @Override
    Character character();
}
