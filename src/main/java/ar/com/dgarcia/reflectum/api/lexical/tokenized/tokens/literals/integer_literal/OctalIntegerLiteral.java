package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal;

import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.IntegerLiteral;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.octal.OctalNumeral;

import java.util.Optional;

/**
 * This type represents an base 8 integer value
 * Created by kfgodel on 03/08/14.
 */
public interface OctalIntegerLiteral extends IntegerLiteral {

    /**
     * @return The numeral octal literal
     */
    OctalNumeral numeral();

    /**
     * @return The type of value
     */
    Optional<IntegerTypeSuffix> suffix();

}
