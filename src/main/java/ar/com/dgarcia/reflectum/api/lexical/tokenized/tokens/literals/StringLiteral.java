package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiDoubleQuote;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.string_literal.StringCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Literal;

import java.util.stream.Stream;

/**
 * This type represents a string literal value as "\"string\""
 * Created by kfgodel on 03/08/14.
 */
public interface StringLiteral extends Literal {

    /**
     * @return The opening marker
     */
    AsciiDoubleQuote opening();

    /**
     * @return The sequence of characters
     */
    Stream<StringCharacter> characters();

    /**
     * @return The closing marker
     */
    AsciiDoubleQuote closing();
}
