package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits;

import java.util.Optional;

/**
 * This type represents a sequence of binary digits and underscores
 * Created by kfgodel on 04/08/14.
 */
public interface UnderscoredBinaryDigits {

    /**
     * @return The starting digit
     */
    BinaryDigit firstDigit();

    /**
     * @return The sequence of digits and underscores
     */
    Optional<BinaryDigitsAndUnderscores> middle();

    /**
     * @return The ending digit
     */
    BinaryDigit lastDigit();
}
