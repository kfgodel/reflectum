package ar.com.dgarcia.reflectum.api.declarations.invocations.constructors;

import ar.com.dgarcia.reflectum.api.declarations.invocations.ExplicitConstructorInvocation;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.pseudo_variables.ThisKeyword;

/**
 * This type represents a local constructor invocation
 * Created by kfgodel on 12/08/14.
 */
public interface LocalConstructorInvocation extends ExplicitConstructorInvocation {

    /**
     * @return The keyword to indicate a local constructor
     */
    ThisKeyword thisKeyword();
}
