package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.binary.digits;

import java.util.stream.Stream;

/**
 * This type represents a sequence of binary digits or underscores
 * Created by kfgodel on 04/08/14.
 */
public interface BinaryDigitsAndUnderscores {

    /**
     * @return Starting symbol of the sequence
     */
    BinaryDigitOrUnderscore firstSymbol();

    /**
     * @return Rest of the sequence
     */
    Stream<BinaryDigitOrUnderscore> tailSymbols();
}
