package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.types;

import ar.com.dgarcia.reflectum.api.declarations.references.primitives.IntegralTypeKeyword;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.Keyword;

/**
 * This type represents the "long" keyword
 * Created by kfgodel on 03/08/14.
 */
public interface LongKeyword extends Keyword, IntegralTypeKeyword {
}
