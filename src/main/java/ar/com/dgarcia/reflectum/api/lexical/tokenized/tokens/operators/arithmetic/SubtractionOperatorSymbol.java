package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.arithmetic;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiMinus;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the substraction operator symbol "-"
 * Created by kfgodel on 05/08/14.
 */
public interface SubtractionOperatorSymbol extends OperatorSymbol {

    AsciiMinus character();
}
