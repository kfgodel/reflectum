package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.decimal.digits;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;

/**
 * This type represents a digit
 * Created by kfgodel on 03/08/14.
 */
public interface Digit extends UnicodeInputCharacter, Digits, DigitOrUnderscore {

    /**
     * @return Any of 0 1 2 3 4 5 6 7 8 9
     */
    @Override
    Character character();
}
