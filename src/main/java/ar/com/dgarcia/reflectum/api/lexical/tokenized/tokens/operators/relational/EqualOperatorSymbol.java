package ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.operators.relational;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.AsciiEqual;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.OperatorSymbol;

/**
 * This type represents the equal operator symbol "=="
 * Created by kfgodel on 05/08/14.
 */
public interface EqualOperatorSymbol extends OperatorSymbol {

    AsciiEqual firstCharacter();
    AsciiEqual secondCharacter();
}
