package ar.com.dgarcia.reflectum.api.declarations.parameters;

/**
 * This type represents the last parameter of a method definition
 * Created by kfgodel on 12/08/14.
 */
public interface LastFormalParameter extends FormalParameterList {
}
