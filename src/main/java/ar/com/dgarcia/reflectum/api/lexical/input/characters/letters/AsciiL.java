package ar.com.dgarcia.reflectum.api.lexical.input.characters.letters;

import ar.com.dgarcia.reflectum.api.lexical.input.unicode.UnicodeInputCharacter;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.literals.integer_literal.IntegerTypeSuffix;

/**
 * This type represents the ascii "L" character
 * Created by kfgodel on 03/08/14.
 */
public interface AsciiL extends UnicodeInputCharacter, IntegerTypeSuffix {

    /**
     * @return The "L" character
     */
    @Override
    Character character();
}
