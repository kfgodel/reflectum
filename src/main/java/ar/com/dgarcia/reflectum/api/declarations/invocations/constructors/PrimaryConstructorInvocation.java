package ar.com.dgarcia.reflectum.api.declarations.invocations.constructors;

import ar.com.dgarcia.reflectum.api.declarations.expressions.Primary;
import ar.com.dgarcia.reflectum.api.declarations.invocations.ExplicitConstructorInvocation;
import ar.com.dgarcia.reflectum.api.lexical.input.characters.separators.AsciiDot;
import ar.com.dgarcia.reflectum.api.lexical.tokenized.tokens.keywords.pseudo_variables.SuperKeyword;

/**
 * This type represents a primary contructor invocation
 * Created by kfgodel on 13/08/14.
 */
public interface PrimaryConstructorInvocation extends ExplicitConstructorInvocation {

    /**
     * @return A primary expression
     */
    Primary primary();

    /**
     * @return The nesting separator
     */
    AsciiDot nestingSeparator();

    /**
     * @return The kewyword to indicate a super constructor
     */
    SuperKeyword superKeyword();
}
