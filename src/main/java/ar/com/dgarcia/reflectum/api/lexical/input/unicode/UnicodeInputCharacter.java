package ar.com.dgarcia.reflectum.api.lexical.input.unicode;

import ar.com.dgarcia.reflectum.api.lexical.input.characters.CharacterSymbol;

/**
 * This type represents one of the code characters that can be parsed by the compiler
 * Created by kfgodel on 03/08/14.
 */
public interface UnicodeInputCharacter extends CharacterSymbol {

}
