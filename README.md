reflectum
==========

Java library to extend reflection capabilities of java standard JDK
(Work in progress)

```
#!java

```

### Maven dependency ###

* Add this repository to your pom:  
```
#!xml
    <repository>
      <id>kfgodel_mosquito</id>
      <name>Repo Mosquito</name>
      <url>http://kfgodel.info:8081/nexus/content/groups/public/</url>
    </repository>
```

* Declare the dependency
```
#!xml

<dependency>
  <groupId>ar.com.dgarcia</groupId>
  <artifactId>reflectum</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```
